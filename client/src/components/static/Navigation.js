import React from 'react';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import { makeStyles } from '@material-ui/core/styles';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import { firestore, googleProvider, facebookProvider, auth } from "../../Firebase/index";
import Avatar from '@material-ui/core/Avatar';
import Snackbar from '@material-ui/core/Snackbar';
import CloseIcon from '@material-ui/icons/Close';
import FavoriteIcon from '@material-ui/icons/Favorite';
import {
    BrowserView,
    MobileView
} from "react-device-detect";
const useStyles = makeStyles({
    list: {
      width: 400,
    },
    fullList: {
      width: 'auto',
    },
});


const Navigation = () => {

    const [open, setOpen] = React.useState(false);
    const [totalItems, setTotalItems] = React.useState(0);
    const [email, setEmail] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [user, setUser] = React.useState({});
    const [userlogin, setUserLogin] = React.useState();
    const [openVerify, setOpenVerify] = React.useState(false);
    const [fullName, setFullName] = React.useState('');
    const [message, setMessage] = React.useState('');
    const [openSnackbar, setOpenSnackbar] = React.useState(false);
    const handleClickSnackbar = () => {
        setOpenSnackbar(true);
    };
    const handleCloseSnackbar = (event, reason) => {
    if (reason === 'clickaway') {
        return;
    }
        setOpenSnackbar(false);
    };


    const handleClickOpenVerify = () => {
        setOpenVerify(true);
    };
    const handleCloseVerify = () => {
        setOpenVerify(false);
    };

    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    const [openSignUp, setOpenSignUp] = React.useState(false);
    const handleClickOpenSignUp = () => {
        setOpenSignUp(true);
    };
    const handleCloseSignUp = () => {
        setOpenSignUp(false);
    };

    const registerOpen = option => {
        if ( option === "Register" ) {
        handleClose();
        handleClickOpenSignUp();
        } else {
        handleCloseSignUp();
        handleClickOpen();
        }
    }

    const signIn = () => {
        auth.signInWithPopup(googleProvider).then((user) => {
        sessionStorage.setItem("userId", user.uid);
        sessionStorage.setItem("userEmail", user.email);
        sessionStorage.setItem("login", true);
        setUserLogin(true);
        handleClose();
        var user2 = auth.currentUser;
        if (user2) {
            cartItemFetch(user2.uid);
        } 
        }).catch(err => console.log(err));
    }

    const signInFacebook = () => {
        auth.signInWithPopup(facebookProvider).then(function(result) {
            sessionStorage.setItem("userId", user.uid);
            sessionStorage.setItem("userEmail", user.email);
            sessionStorage.setItem("login", true);
            setUserLogin(true);
            handleClose();
            var user2 = auth.currentUser;
            if (user2) {
                cartItemFetch(user2.uid);
            } 
        }).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            // ...
            console.log(errorMessage);
        });
    }

    React.useEffect(() => {
        auth.onAuthStateChanged(function(user) {
        if (user) {
            setUser(user);
            sessionStorage.setItem("userId", user.uid);
            sessionStorage.setItem("userEmail", user.email);
            sessionStorage.setItem("login", true);
            setUserLogin(true);
            cartItemFetch(user.uid);
        } else {
            setUser({});
            sessionStorage.setItem("login", false);
            sessionStorage.setItem("userId", false);
            sessionStorage.setItem("userEmail", false);
            setUserLogin(false);
        }
        });
    }, []);

    React.useEffect(() => {
        if (sessionStorage.getItem("login")) {
        var uid = sessionStorage.getItem("userId");
        firestore.collection("cart").doc(uid)
        .onSnapshot(function(doc) {
            if (typeof(doc.data())  == 'undefined') {
                setTotalItems(0);
            } else {
                setTotalItems(doc.data().items);
            }
        });
        }
    }, []);

    const cartItemFetch = (uid) => {
        firestore.collection("cart").doc(uid)
        .onSnapshot(function(doc) {
            if (typeof(doc.data())  == 'undefined') {
                setTotalItems(0);
            } else {
                setTotalItems(doc.data().items);
            }
        });
    }

    const logOut = () => {
        auth.signOut().then(function() {
            setUser({});
            sessionStorage.setItem("login", false);
            sessionStorage.setItem("userId", false);
            sessionStorage.setItem("userEmail", false);
            setTotalItems(0);
            setUserLogin(false);
        }).catch(function(error) {
            console.log(error);
        });
    }


    const signUp = () => {
        auth.createUserWithEmailAndPassword(email, password)
        .then((result) => {
            var user = result.user;
            user.updateProfile({
                photoURL: "https://kittyinpink.co.uk/wp-content/uploads/2016/12/facebook-default-photo-male_1-1.jpg",
                displayName: fullName
            })
            .then(() => {
              user.sendEmailVerification().then(function() {
                handleClickOpenVerify();
                handleCloseSignUp();
              }).catch(function(error) {
                  console.log(error);
              });
            })
            .catch(err => console.log(err))
        })
        .catch(function(error) {
            if (error.code === "auth/email-already-in-use") {
              setMessage("This email is already registered");
              setPassword('');
              setEmail('');
            }
        });
    }

    const login = () => {
        auth.signInWithEmailAndPassword(email, password)
        .then(() => {
            auth.onAuthStateChanged(function(user) {
                if (user) {
                    setUser(user);
                    sessionStorage.setItem("userId", user.uid);
                    sessionStorage.setItem("userEmail", user.email);
                    sessionStorage.setItem("login", true);
                    setUserLogin(true);
                    cartItemFetch(user.uid);
                    handleCloseSnackbar();
                } else {
                    console.log("No User");
                }
            });
        })
        .catch(function(error) {
            var errorCode = error.code;
            if ( errorCode === "auth/wrong-password" ) {
              setMessage("Wrong Password");
              setPassword('');
              setEmail('');
              handleClickSnackbar();
            } else if ( errorCode === "auth/user-not-found" ) {
              setMessage("No User Found");
              setPassword('');
              setEmail('');
              handleClickSnackbar();
            }
        });
    }

    return (
        <>
            <Snackbar
                anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
                }}
                open={openSnackbar}
                autoHideDuration={1000}
                onClose={handleCloseSnackbar}
                message={message}
                action={
                <React.Fragment>
                    <IconButton size="small" aria-label="close" color="inherit" onClick={handleCloseSnackbar}>
                    <CloseIcon fontSize="small" />
                    </IconButton>
                </React.Fragment>
                }
            />

            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Login</DialogTitle>
                <DialogContent>
                <DialogContentText style={{ color: '#000000', fontSize: '2vh' }}>
                    Please Logged In to this website, your email and password are end to end encrypted and secure.
                </DialogContentText>
                <TextField value={email} onChange={(event) => setEmail(event.target.value)} style={{ marginTop: '4px' }} id="outlined-basic" label="Email" type="email" variant="outlined" fullWidth />
                <br />
                <TextField value={password} onChange={(event) => setPassword(event.target.value)} style={{ marginTop: '8px', marginBottom: '10px' }} id="outlined-basic" label="Password" type="password" variant="outlined" fullWidth />
                
                <Button onClick={login} style={{ marginTop: '14px', backgroundColor: '#000000', color: '#ffffff', fontSize: '2vh', marginBottom: '10px' }} size="large" startIcon={<ExitToAppIcon />} variant="outlined" fullWidth >Login</Button>
                <center style={{ marginTop: '10px' }}>
                    OR
                </center>
                <Button onClick={signIn} style={{ backgroundColor: '#ffffff', fontSize: '2vh', color: '#000000', size: '10px', height: '50px',  marginTop: '10px' }} variant="contained" fullWidth>
                    <img alt="DocsUp" src="https://img.icons8.com/color/28/000000/google-logo.png"/>
                    Continue with Google
                </Button>
                <Button onClick={signInFacebook} style={{ backgroundColor: '#ffffff', fontSize: '2vh', color: '#000000', size: '10px', height: '50px',  marginTop: '10px' }} variant="contained" fullWidth>
                    <img alt="DocsUp" src="https://img.icons8.com/color/28/000000/facebook-new.png"/>
                    Continue with Facebook
                </Button>
                
                <center style={{ marginTop: '20px', marginBottom: '10px' }}>
                    <a onClick={() => registerOpen('Register')} href="#!">
                    Don't have an Account Sign Up
                    </a>
                </center>

                </DialogContent>
                <DialogActions>
                <Button onClick={handleClose} color="primary">
                    Close
                </Button>
                </DialogActions>
            </Dialog>

            <Dialog open={openVerify} onClose={handleCloseVerify} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Email Verification</DialogTitle>
                <DialogContent>
                <DialogContentText style={{ color: '#000000', fontSize: '3vh' }}>
                <center>
                    Please Check Your Email Inbox for the Verification Link
                </center>
                </DialogContentText>
                <center>
                    <img src="https://cdn.dribbble.com/users/4874/screenshots/1776423/inboxiconanimation_30.gif" className="img img-fluid w-50" />
                </center>
                <br />
                <center style={{ marginTop: '20px', marginBottom: '10px' }}>
                    <a onClick={() => registerOpen('Login')} href="#!">
                    Back to Login
                    </a>
                </center>
                </DialogContent>
                <DialogActions>
                <Button onClick={handleCloseVerify} color="primary">
                    Close
                </Button>
                </DialogActions>
            </Dialog>

            <Dialog open={openSignUp} onClose={handleCloseSignUp} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Sign Up</DialogTitle>
                <DialogContent>
                <DialogContentText style={{ color: '#000000', fontSize: '2vh' }}>
                    Please Register into this website, your email and password are end to end encrypted and secure.
                </DialogContentText>
                <TextField value={fullName} onChange={(event) => setFullName(event.target.value)} style={{ marginTop: '4px' }} id="outlined-basic" label="Full Name" type="text" variant="outlined" fullWidth />
                <br />
                <TextField onChange={(event) => setEmail(event.target.value)} style={{ marginTop: '8px', marginBottom: '10px' }} id="outlined-basic" label="Email" type="email" variant="outlined" fullWidth />
                <TextField onChange={(event) => setPassword(event.target.value)} style={{ marginTop: '8px', marginBottom: '10px' }} id="outlined-basic" label="Password" type="password" variant="outlined" fullWidth />
                
                <Button onClick={signUp} style={{ marginTop: '14px', fontSize: '2vh', backgroundColor: '#000000', color: '#ffffff' , marginBottom: '10px' }} size="large" startIcon={<ExitToAppIcon />} variant="outlined" fullWidth >Sign Up</Button>
                
                <center style={{ marginTop: '20px', marginBottom: '10px' }}>
                    <a onClick={() => registerOpen('Login')} href="#!">
                    Already have an account
                    </a>
                </center>

                </DialogContent>
                <DialogActions>
                <Button onClick={handleCloseSignUp} color="primary">
                    Close
                </Button>
                </DialogActions>
            </Dialog>

            <BrowserView>
                <header id="header">
                    <div id="nav" className="fixed-top">
                        <div id="nav-top" style={{ backgroundColor: '#fff' }}>
                            <div class="container">
                                <ul class="nav-social">
                                    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                </ul>
                                <div class="nav-logo">
                                    <a href="index.html" className="logo navigationheading">GoWeed</a>
                                </div>

                                <div className="nav-btns">
                                    {
                                        userlogin ? (
                                        <>
                                            <Avatar className="search-btn" style={{ cursor: 'pointer'}} onClick={() => window.location.href = "/profile"} alt={user.name} src={user.photoURL} />
                                        </>
                                        ) : null
                                    }
                                </div>

                                <div class="nav-btns">
                                    <div className="row">
                                        <div className="col">
                                            {
                                                userlogin ? (
                                                <IconButton style={{ color: '#000' }} href="/favoriteitem" aria-label="delete">
                                                    <FavoriteIcon color="secondary" fontSize="large" />
                                                </IconButton>
                                                ) : null
                                            }
                                        </div>
                                        <div className="col">
                                            {
                                                userlogin ? (
                                                <div style={{ marginRight: '6vh' }}>
                                                <Badge badgeContent={totalItems} color="secondary">
                                                    <IconButton style={{ color: '#000' }} href="/cart" aria-label="delete">
                                                        <ShoppingCartIcon fontSize="large" />
                                                    </IconButton>
                                                </Badge>
                                                </div>
                                                ) : (
                                                <a className="navbar-item is-hidden-desktop-only" href="#!">
                                                    <IconButton onClick={handleClickOpen} aria-label="delete">
                                                        <ExitToAppIcon fontSize="large" style={{ color: '#000' }} />
                                                    </IconButton>
                                                </a>
                                                )
                                            }
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        
                        <section>
                            <div class="menu-container">
                            <div class="menu">
                                <ul style={{ paddingLeft: '16%' }}>
                                <li><a href="/home">Home</a></li>
                                <li><a href="/products">Flowers</a>
                                </li>
                                <li><a href="/prerolls">Pre Rolls</a></li>
                                <li><a href="/vapes">Vapes</a>
                                </li>
                                <li><a href="/extracts">Extracts</a>
                                </li>
                                <li><a href="/edibles">Edibles</a>
                                </li>
                                <li><a href="/tropicals">Topicals</a>
                                </li>
                                <li><a href="/recentbuys">Recents Buys</a>
                                </li>
                                </ul>
                            </div>
                            </div>
                        </section>
                    </div>
                </header>
                <br />
                <br />
                <br />
                <br />
                <br />
            </BrowserView>
            <MobileView>
            <header id="header">
                <div id="nav">
                    <div id="nav-top">
                        <div class="container">
                            <ul class="nav-social">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                            <div class="nav-logo">
                                <a href="index.html" style={{ fontSize: '4vh' }} class="logo">Goweed.ca</a>
                            </div>

                            <div className="nav-btns">
                                {
                                    userlogin ? (
                                    <>
                                        <Avatar className="search-btn" style={{ cursor: 'pointer'}} onClick={() => window.location.href = "/profile"} alt={user.name} src={user.photoURL} />
                                    </>
                                    ) : null
                                }
                            </div>

                            <div class="nav-btns">
                                <div className="row">
                                    <div className="col">
                                        {
                                            userlogin ? (
                                            <IconButton style={{ color: '#000' }} href="/favoriteitem" aria-label="delete">
                                                <FavoriteIcon color="secondary" fontSize="large" />
                                            </IconButton>
                                            ) : null
                                        }
                                    </div>
                                    <div className="col">
                                        {
                                            userlogin ? (
                                            <div style={{ marginRight: '3vh' }}>
                                            <Badge badgeContent={totalItems} color="secondary">
                                                <IconButton style={{ color: '#000' }} href="/cart" aria-label="delete">
                                                    <ShoppingCartIcon fontSize="large" />
                                                </IconButton>
                                            </Badge>
                                            </div>
                                            ) : (
                                            <a className="navbar-item is-hidden-desktop-only" href="#!">
                                                <IconButton onClick={handleClickOpen} aria-label="delete">
                                                    <ExitToAppIcon fontSize="large" style={{ color: '#000' }} />
                                                </IconButton>
                                            </a>
                                            )
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <section>
                        <div class="menu-container">
                        <div class="menu">
                            <ul>
                                <li><a href="/home">Home</a></li>
                                <li><a href="/products">Flowers</a>
                                </li>
                                <li><a href="/prerolls">Pre Rolls</a></li>
                                <li><a href="/vapes">Vapes</a>
                                </li>
                                <li><a href="/extracts">Extracts</a>
                                </li>
                                <li><a href="/edibles">Edibles</a>
                                </li>
                                <li><a href="/tropicals">Topicals</a>
                                </li>
                                <li><a href="/recentbuys">Recents Buys</a>
                                </li>
                            </ul>
                        </div>
                        </div>
                    </section>
                </div>
            </header>
            </MobileView>
            
        </>
    )
}

export default Navigation
