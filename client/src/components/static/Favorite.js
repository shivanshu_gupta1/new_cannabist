import React from 'react';
import { auth } from "../../Firebase/index";
import axios from 'axios';
import { API_SERVICE } from '../../config/URI';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import MuiAlert from '@material-ui/lab/Alert';


function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}


const Favorite = ({ product }) => {
    const [userlogin, setUserLogin] = React.useState(false);
    const [openMessage, setOpen] = React.useState(false);
    const [message, setMessage] = React.useState(null);

    const [state, setState] = React.useState({
        open: false,
        vertical: 'top',
        horizontal: 'right',
      });

    const handleClick = () => {
        setOpen(true);
    };

    const { vertical, horizontal, open } = state;
    
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setOpen(false);
    };

    const addToFavorite = () => {
        const useremail = sessionStorage.getItem("userEmail");
        var uploadData = {
            useremail,
            product
        }
        axios.post(`${API_SERVICE}/api/v1/main/addtofavorite`, uploadData)
            .then((response) => {
                if (response.status === 200) {
                    handleClick();
                    setMessage('Item Added Favorite');
                } 
            }).catch(err => console.log(err));
    }


    React.useEffect(() => {
        auth.onAuthStateChanged(function(user) {
        if (user) {
            setUserLogin(true);
        } else {
            setUserLogin(false);
        }
        });
    }, []);


    return (
        <>
            <Snackbar
                anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
                }}
                open={openMessage}
                autoHideDuration={4000}
                onClose={handleClose}
                message={message}
                canchorOrigin={{ vertical, horizontal }}
                action={
                <React.Fragment>
                    <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
                    <CloseIcon fontSize="small" />
                    </IconButton>
                </React.Fragment>
                }
            >
                <Alert onClose={handleClose}>
                    <h5 style={{ color: '#fff' }}>{message}</h5>
                </Alert>
            </Snackbar>

            {
                userlogin ? (
                    <button onClick={addToFavorite} style={{ fontSize: '2vh', border: 'none', fontWeight: 'bold'}} className="btn btn-block btn-lg mt-2 btn-danger">
                        Add to <i style={{ fontSize: '2vh', fontWeight: 'bold'}} className="fas fa-heart"></i> 
                    </button>
                ) : null
            }
        </>
    )
}

export default Favorite
