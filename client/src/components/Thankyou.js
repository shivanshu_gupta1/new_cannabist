import React from 'react';
// Components
import Navigation from './static/Navigation';
import Footer from './static/Footer';
// API Service
import { API_SERVICE, SECRET_KEY } from '../config/URI';
import Snackbar from '@material-ui/core/Snackbar';
import CloseIcon from '@material-ui/icons/Close';
import MuiAlert from '@material-ui/lab/Alert';
import IconButton from '@material-ui/core/IconButton';
import axios from 'axios';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

const Products = ({ location }) => {
    const [message, setmessage] = React.useState('');
    const [open, setOpen] = React.useState(false);

    return (
        <>
            <Navigation />
            <div style={{ backgroundColor: 'gainsboro' }} className="jumbotron jumbotron-fluid">
                <div className="container text-center">
                    <h2 className="display-4 font-weight-bold">Order Successfully Placed</h2>
                </div>
            </div>
            <section className="m-4 mt-5 mb-5">
                <h2 className="text-center">
                    Your order is <b className="text-success">Successfully</b> placed and will get delivered to you within 24 hours of time.
                </h2>

                <center>
                    <a href="/trackorder" style={{ fontSize: '2vh' }} className="btn btn-info btn-lg">
                        Track My Order
                    </a>
                </center>

                <center className="mt-5 mb-5">
                    <img src="https://i.pinimg.com/originals/ba/56/00/ba5600e2613495b885f953f41c120d11.gif" />
                </center>
            </section>
            <Footer />
        </>
    )
}

export default Products
