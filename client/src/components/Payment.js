import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import queryString from 'query-string';
import NumberFormat from 'react-number-format';
// Stripe
import {Elements} from '@stripe/react-stripe-js';
import {loadStripe} from '@stripe/stripe-js';
import HomePage from './HomePage';
// URI
import { API_SERVICE, SECRET_KEY } from '../config/URI';
import CircularProgress from '@material-ui/core/CircularProgress';


const stripePromise = loadStripe('pk_test_51IHaYPEr6dpmT8oAFHgW8JYU6k1ejYhuS7PLfqAvgfZNHdYfxK8TjZhWu2s2OREsrj2uklC83DlSXhtKuqpHho5b00UKysp5ik');


export default function Payment({ location }) {
    const [loading, setLoading] = React.useState(true);

    return (
        <React.Fragment>
        <Container maxWidth="lg">
            {
                  loading === true ? (
                      <center style={{ marginTop: '10%', marginLeft: '50%' }}>
                          <CircularProgress />
                      </center>
                  ) : (
                    <Elements stripe={stripePromise}>
                      <HomePage type={'vg'} amount={2000} />
                    </Elements>
                  )
              }
        </Container>
        </React.Fragment>
    );
}