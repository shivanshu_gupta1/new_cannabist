import React from 'react'

// Components
import Navigation from './static/Navigation';
import Footer from './static/Footer';

const Home = () => {
    return (
        <>
            <Navigation />
            
            <center>
                <img className="mt-5" src="https://res.cloudinary.com/dx9dnqzaj/image/upload/v1614106249/cannabis/1j_ojFVDOMkX9Wytexe43D6khvaArh9OnhrNwXs1M3EMoAJtliEph.._twtjlx.png" />
            </center>
            <section className="container w-100" style={{ marginTop: '2%' }}>
                <h2 className="text-center font-weight-bold mb-5">
                EXPLORE OUR STORE
                </h2>

                <div className="row">
                    <div className="col-md">
                        <div class="card border-0">
                        <center>
                        <a href="/products?q=sativa">
                            <img style={{ height: '38vh' }} class="shadow card-img-top w-75" src="//cdn.shopify.com/s/files/1/2636/1928/files/CLEARANCE_300x300_crop_center.jpg" alt="Card image cap" />
                        </a>
                        </center>
                        <div class="card-body">
                            <a style={{ fontSize: '2vh', height: '50px' }} href="/products" class="btn newbutton btn-block btn-outline-success">Flower</a>
                        </div>
                        </div>
                    </div>
                    <div className="col-md">
                        <div class="card border-0">
                        <center>
                        <a href="/prerolls">
                        <img style={{ height: '38vh' }} class="shadow card-img-top w-75" src="https://www.marijuana-seeds.nl/wp/wp-content/uploads/2019/05/Finny_4.jpg" alt="Card image cap" />
                        </a>
                        </center>
                        <div class="card-body">
                            <a style={{ fontSize: '2vh', height: '50px' }} href="/prerolls" class="btn newbutton btn-block btn-outline-success">Pre Rolls</a>
                        </div>
                        </div>
                    </div>
                    <div className="col-md">
                        <div class="card border-0">
                        <center>
                        <a href="/vapes">
                        <img style={{ height: '38vh' }} class="shadow card-img-top w-75" src="//cdn.shopify.com/s/files/1/2636/1928/files/VAPES_bb87a19f-16b1-406d-bcaa-83cb4bf14881_300x300_crop_center.jpg?v=1613660412" alt="Card image cap" />
                        </a>
                        </center>
                        <div class="card-body">
                            <a style={{ fontSize: '2vh', height: '50px' }} href="/vapes" class="btn newbutton btn-block btn-outline-success">Vapes</a>
                        </div>
                        </div>
                    </div>
                </div>

                <div className="row mt-2">
                    <div className="col-md">
                        <div class="card border-0">
                        <center>
                        <a href="/edibles">
                            <img style={{ height: '38vh' }} class="shadow card-img-top w-75" src="//cdn.shopify.com/s/files/1/2636/1928/files/MegaNav-Tile-Edibles_227589bb-b37b-4e73-ae8f-7b1770427649_400x.jpg" alt="Card image cap" />
                        </a>
                        </center>
                        <div class="card-body">
                            <a style={{ fontSize: '2vh', height: '50px' }} href="/edibles" class="btn newbutton btn-block btn-outline-success">Edibles</a>
                        </div>
                        </div>
                    </div>
                    <div className="col-md">
                        <div class="card border-0">
                        <center>
                        <a href="/extracts">
                        <img style={{ height: '38vh' }} class="shadow card-img-top w-75" src="https://cdn.shopify.com/s/files/1/2636/1928/products/00688083002762_a1c1_compress_320021_1024x1024.jpg" alt="Card image cap" />
                        </a>
                        </center>
                        <div class="card-body">
                            <a style={{ fontSize: '2vh', height: '50px' }} href="/extracts" class="btn newbutton btn-block btn-outline-success">Beverages</a>
                        </div>
                        </div>
                    </div>
                    <div className="col-md">
                        <div class="card border-0">
                        <center>
                        <a href="/tropicals">
                        <img style={{ height: '38vh' }} class="shadow card-img-top w-75" src="https://cdn.shopify.com/s/files/1/2636/1928/products/00800447000355_00_compressed_101006_1024x1024.jpg" alt="Card image cap" />
                        </a>
                        </center>
                        <div class="card-body">
                            <a style={{ fontSize: '2vh', height: '50px' }} href="/tropicals" class="btn newbutton btn-block btn-outline-success">Capsules</a>
                        </div>
                        </div>
                    </div>
                </div>

                <div className="row mt-2">
                    <div className="col-md">
                        <div class="card border-0">
                        <center>
                        <a href="/edibles">
                            <img style={{ height: '38vh' }} class="shadow card-img-top w-75" src="//cdn.shopify.com/s/files/1/2636/1928/files/18._CBD-MegaNav-Tile_400x.jpg?v=1605190860" alt="Card image cap" />
                        </a>
                        </center>
                        <div class="card-body">
                            <a style={{ fontSize: '2vh', height: '50px' }} href="/edibles" class="btn newbutton btn-block btn-outline-success">Oils</a>
                        </div>
                        </div>
                    </div>
                    <div className="col-md">
                        <div class="card border-0">
                        <center>
                        <a href="/extracts">
                        <img style={{ height: '38vh' }} class="shadow card-img-top w-75" src="//cdn.shopify.com/s/files/1/2636/1928/files/ROSIN_300x300_crop_center.jpg?v=1613660336" alt="Card image cap" />
                        </a>
                        </center>
                        <div class="card-body">
                            <a style={{ fontSize: '2vh', height: '50px' }} href="/extracts" class="btn newbutton btn-block btn-outline-success">Concentrate</a>
                        </div>
                        </div>
                    </div>
                    <div className="col-md">
                        <div class="card border-0">
                        <center>
                        <a href="/tropicals">
                        <img style={{ height: '38vh' }} class="shadow card-img-top w-75" src="//cdn.shopify.com/s/files/1/2636/1928/files/MegaNav-Tile-Topicals_400x.jpg?v=1613663675" alt="Card image cap" />
                        </a>
                        </center>
                        <div class="card-body">
                            <a style={{ fontSize: '2vh', height: '50px' }} href="/tropicals" class="btn newbutton btn-block btn-outline-success">Topicals</a>
                        </div>
                        </div>
                    </div>
                </div>

                {/* <div className="row mt-2">
                    <div className="col-md">
                        <div class="card border-0">
                        <center>
                        <a href="/products?q=sativa">
                            <img style={{ height: '38vh' }} class="shadow card-img-top w-75" src="https://cdn.shopify.com/s/files/1/2636/1928/products/00688083000300_00__compress_100968_1024x1024.jpg?v=1608677029" alt="Card image cap" />
                        </a>
                        </center>
                        <div class="card-body">
                            <a style={{ fontSize: '2vh', height: '50px' }} href="/products?q=sativa" class="btn newbutton btn-block btn-outline-success">Seeds</a>
                        </div>
                        </div>
                    </div>
                    <div className="col-md">
                        <div class="card border-0">
                        <center>
                        <a href="/products?q=sativa">
                        <img style={{ height: '38vh' }} class="shadow card-img-top w-75" src="//cdn.shopify.com/s/files/1/2636/1928/files/LAST-CHANCE_300x300_crop_center.jpg?v=1613660689" alt="Card image cap" />
                        </a>
                        </center>
                        <div class="card-body">
                            <a style={{ fontSize: '2vh', height: '50px' }} href="/products?q=sativa" class="btn newbutton btn-block btn-outline-success">Sprays</a>
                        </div>
                        </div>
                    </div>
                    <div className="col-md">
                        <div class="card border-0">
                        <center>
                        <a href="/products?q=sativa">
                        <img style={{ height: '38vh' }} class="shadow card-img-top w-75" src="//cdn.shopify.com/s/files/1/2636/1928/files/PRICE-DROPS_300x300_crop_center.jpg?v=1613660617" alt="Card image cap" />
                        </a>
                        </center>
                        <div class="card-body">
                            <a style={{ fontSize: '2vh', height: '50px' }} href="/products?q=sativa" class="btn newbutton btn-block btn-outline-success">Accessories</a>
                        </div>
                        </div>
                    </div>
                </div> */}


                
            </section>

            

            <div class="container2" style={{ marginTop: '5%' }}>
                <img className="w-100" src="//cdn.shopify.com/s/files/1/2636/1928/files/Ounces-Under-100-BB-Desktop_1630x500.jpg" />
                <div className="centered">
                    <span  style={{ fontSize: '3vw', fontWeight: 'bold' }}>Delivery Within 24 Hours</span> <br />
                    <span  style={{ fontSize: '3vw', fontWeight: 'bold' }}>That's not a typo. You really can get 28 g of bud for less than $4/g</span>
                </div>
            </div> 

            <section className="container w-100" style={{ marginTop: '5%', marginBottom: '5%' }}>
                <h1 className="text-center font-weight-bold">
                Going Fast
                </h1>

                <div className="row" style={{ marginTop: '5%' }}>
                    <div className="col-md">
                        <div class="card border-0">
                        <center>
                        <img class="shadow card-img-top w-75" src="//cdn.shopify.com/s/files/1/2636/1928/files/PRICE-DROPS_300x300_crop_center.jpg?v=1613660617" alt="Card image cap" />
                        </center>
                        <div class="card-body">
                            <a href="#" class="btn newbutton btn-block btn-outline-success">PRICE DROPS</a>
                        </div>
                        </div>
                    </div>
                    <div className="col-md">
                        <div class="card border-0">
                        <center>
                        <img class="shadow card-img-top w-75" src="//cdn.shopify.com/s/files/1/2636/1928/files/LAST-CHANCE_300x300_crop_center.jpg?v=1613660689" alt="Card image cap" />
                        </center>
                        <div class="card-body">
                            <a href="#" class="btn newbutton btn-block btn-outline-success">LAST CHANCE</a>
                        </div>
                        </div>
                    </div>
                    <div className="col-md">
                        <div class="card border-0">
                        <center>
                        <img class="shadow card-img-top w-75" src="//cdn.shopify.com/s/files/1/2636/1928/files/CLEARANCE_300x300_crop_center.jpg?v=1613661650" alt="Card image cap" />
                        </center>
                        <div class="card-body">
                            <a href="#" class="btn newbutton btn-block btn-outline-success">BESTSELLERS</a>
                        </div>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container2" style={{ marginTop: '5%' }}>
                <img className="w-100" src="//cdn.shopify.com/s/files/1/2636/1928/files/CBD-BB-Desktop_1630x500.jpg" />
                <div className="centered">
                    <span  style={{ fontSize: '3vw', fontWeight: 'bold' }}>Shop CBD</span> <br />
                    <span  style={{ fontSize: '3vw', fontWeight: 'bold' }}>Find the CBD oils, flower and capsules you're looking for</span>
                </div>
            </div> 
            <Footer  />
        </>
    )
}

export default Home
