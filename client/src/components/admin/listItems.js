import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import DashboardIcon from '@material-ui/icons/Dashboard';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import PeopleIcon from '@material-ui/icons/People';
import AddIcon from '@material-ui/icons/Add';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
export const mainListItems = (
  <div>
    <ListItem onClick={() => window.location.href = "/admin/orders" } button>
      <ListItemIcon>
        <ShoppingCartIcon />
      </ListItemIcon>
      <ListItemText primary="Orders" />
    </ListItem>
    <ListItem onClick={() => window.location.href = "/admin/addproducts" } button>
      <ListItemIcon>
        <AddIcon />
      </ListItemIcon>
      <ListItemText primary="Add Flowers" />
    </ListItem>
    <ListItem onClick={() => window.location.href = "/admin/addprerolls" } button>
      <ListItemIcon>
        <AddIcon />
      </ListItemIcon>
      <ListItemText primary="Add Pre Rolls" />
    </ListItem>
    <ListItem onClick={() => window.location.href = "/admin/addvapes" } button>
      <ListItemIcon>
        <AddIcon />
      </ListItemIcon>
      <ListItemText primary="Add Vapes" />
    </ListItem>
    <ListItem onClick={() => window.location.href = "/admin/addextracts" } button>
      <ListItemIcon>
        <AddIcon />
      </ListItemIcon>
      <ListItemText primary="Add Extracts" />
    </ListItem>
    <ListItem onClick={() => window.location.href = "/admin/addedibles" } button>
      <ListItemIcon>
        <AddIcon />
      </ListItemIcon>
      <ListItemText primary="Add Edibles" />
    </ListItem>
    <ListItem onClick={() => window.location.href = "/admin/addtropicals" } button>
      <ListItemIcon>
        <AddIcon />
      </ListItemIcon>
      <ListItemText primary="Add Tropicals" />
    </ListItem>
  </div>
);