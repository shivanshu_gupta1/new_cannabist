import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import Box from '@material-ui/core/Box';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import { mainListItems } from './listItems';
import queryString from 'query-string';
import { API_SERVICE } from '../../config/URI';
import axios from 'axios';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import Dropzone from 'react-dropzone';
import CloseIcon from '@material-ui/icons/Close';
import Snackbar from '@material-ui/core/Snackbar';
import { v4 as uuid4 } from 'uuid';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
// Firebase
import { storage } from '../../Firebase/index';

const drawerWidth = 240;
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
  pos: {
    marginBottom: 12,
    color: '#000000',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: '3vh'
  },
  cardroot: {
      minWidth: 230,
  },
}));

export default function NewPreRolls({ location }) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const [name, setname] = React.useState('');
  const [company, setcompany] = React.useState('');
  const [thc, setthc] = React.useState('');
  const [cbd, setcbd] = React.useState('');
  const [category, setcategory] = React.useState('');
  const [price, setprice] = React.useState('');
  
  const [file, setFile] = React.useState([]);
  const [file2, setFile2] = React.useState([]);
  const [message, setMessage] = React.useState('');
  const [photoDownloadUrl1, setphotoDownloadUrl1] = React.useState('das');
  const [photoDownloadUrl2, setphotoDownloadUrl2] = React.useState('dasd');

  const [fields, setFields] = React.useState([{ size: null}]);

  function handleChange(i, event) {
    const values = [...fields];
    values[i].size = event.target.value;
    setFields(values);
  }

  function handleAdd() {
    const values = [...fields];
    values.push({ value: null });
    setFields(values);
  }

  function handleRemove(i) {
    const values = [...fields];
    values.splice(i, 1);
    setFields(values);
  }

  React.useEffect(() => {
      if (file.length > 0) {
          onSubmit();
      } else {
          console.log("N");
      }
  }, [file])

  React.useEffect(() => {
      if (file2.length > 0) {
          onSubmit2();
      } else {
          console.log("N");
      }
  }, [file2])
  
  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };
  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);


  const [openSnackbar, setOpenSnackbar] = React.useState(false);
  const handleClickSnackbar = () => {
    setOpenSnackbar(true);
  };
  const handleCloseSnackbar = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenSnackbar(false);
  };
  React.useEffect(() => {
    const { n } = queryString.parse(location.search);
    if (n === 's') {
      handleClickSnackbar();
      setMessage('Added Successfully');
    } 
  }, []);


  const addFlower = () => {
    var uploadData = {
      name,
      company,
      thc,
      cbd,
      category,
      photoDownloadUrl1,
      price,
      size: fields
    }
    axios.post(`${API_SERVICE}/api/v1/main/addprerolls`, uploadData)
        .then((response) => {
            
        }).catch(err => console.log(err));
        setTimeout(function(){ 
            window.location.href = "/admin/addprerolls";
        }, 1500);
  }

  const handleDrop = async (acceptedFiles) => {
      setFile(acceptedFiles.map(file => file));
  }

  const handleDrop2 = async (acceptedFiles) => {
      setFile2(acceptedFiles.map(file => file));
  }
  const onSubmit = () => {
      if (file.length > 0) {
          file.forEach(file => {
              var file_name = file.name; 
              var fileExtension = file_name.split('.').pop();
              if ( fileExtension === 'png' || fileExtension === 'jpeg' || fileExtension === 'jpg' || fileExtension === 'PNG' || fileExtension === 'JPG' ) {
                  var uniquetwoKey = uuid4();
                  const uploadTask = storage.ref(`photos/${uniquetwoKey}/${file.name}`).put(file);
                  uploadTask.on('state_changed', (snapshot) => {
                      const progress =  Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                      handleClickSnackbar();
                      setMessage(`Uploading ${progress} %`);
                  },
                  (error) => {
                      setMessage(error);
                      handleClickSnackbar();
                  },
                  async () => {
                      // When the Storage gets Completed
                      const filePath = await uploadTask.snapshot.ref.getDownloadURL();
                      handleClickSnackbar();
                      setMessage('File Uploaded');
                      setphotoDownloadUrl1(filePath);
                  });
              } else {
                  handleClickSnackbar();
                  setMessage('Please Upload Images Only');
              }
          })
      } else {
          setMessage('No File Selected Yet');
      }
  }

  const onSubmit2 = () => {
      if (file2.length > 0) {
          file2.forEach(file => {
              var file_name = file.name; 
              var fileExtension = file_name.split('.').pop();
              if ( fileExtension === 'png' || fileExtension === 'jpeg' || fileExtension === 'jpg' || fileExtension === 'PNG' || fileExtension === 'JPG' ) {
                  var uniquetwoKey = uuid4();
                  const uploadTask = storage.ref(`photos/${uniquetwoKey}/${file.name}`).put(file);
                  uploadTask.on('state_changed', (snapshot) => {
                      const progress =  Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
                      handleClickSnackbar();
                      setMessage(`Uploading ${progress} %`);
                  },
                  (error) => {
                      setMessage(error);
                      handleClickSnackbar();
                  },
                  async () => {
                      // When the Storage gets Completed
                      const filePath = await uploadTask.snapshot.ref.getDownloadURL();
                      handleClickSnackbar();
                      setMessage('File Uploaded');
                      setphotoDownloadUrl2(filePath);
                  });
              } else {
                  handleClickSnackbar();
                  setMessage('Please Upload Images Only');
              }
          })
      } else {
          setMessage('No File Selected Yet');
      }
  }

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={openSnackbar}
        autoHideDuration={4000}
        onClose={handleCloseSnackbar}
        message={message}
        action={
          <React.Fragment>
            <IconButton size="small" aria-label="close" color="inherit" onClick={handleCloseSnackbar}>
              <CloseIcon fontSize="small" />
            </IconButton>
          </React.Fragment>
        }
      />
      <AppBar position="absolute" className={clsx(classes.appBar, open && classes.appBarShift)}>
        <Toolbar className={classes.toolbar}>
          <IconButton
            edge="start"
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            className={clsx(classes.menuButton, open && classes.menuButtonHidden)}
          >
            <MenuIcon />
          </IconButton>
          <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
            Dashboard
          </Typography>
          
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        classes={{
          paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
        }}
        open={open}
      >
        <div className={classes.toolbarIcon}>
          <IconButton onClick={handleDrawerClose}>
            <ChevronLeftIcon />
          </IconButton>
        </div>
        <Divider />
        <List>{mainListItems}</List>
      </Drawer>
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="lg" className={classes.container}>
          <h2 className="font-weight-bold mt-2 mb-4">
              Add Pre Rolls
          </h2>
          
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <TextField
                  variant="outlined"
                  required
                  fullWidth
                  label="Name"
                  onChange={(event) => setname(event.target.value)}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                  variant="outlined"
                  required
                  fullWidth
                  label="Company"
                  onChange={(event) => setcompany(event.target.value)}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                  variant="outlined"
                  required
                  fullWidth
                  label="THC"
                  onChange={(event) => setthc(event.target.value)}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                  variant="outlined"
                  required
                  fullWidth
                  label="CBD"
                  onChange={(event) => setcbd(event.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                  variant="outlined"
                  required
                  fullWidth
                  label="Price"
                  onChange={(event) => setprice(event.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <FormControl style={{ width: '100%' }}>
              <InputLabel style={{ fontSize: '2vh', color: '#000000' }} id="demo-simple-select-label">Category</InputLabel>
              <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select-outlined"
                  fullWidth
                  style={{ fontSize: '2vh', color: '#000000' }} 
                  onChange={(event) => setcategory(event.target.value)}
              >
              <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='Indica'>Indica</MenuItem>
              <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='Sativa'>Sativa</MenuItem>
              <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='Hybrid'>Hybrid</MenuItem>
              </Select>
              </FormControl>
            </Grid>
          </Grid>
          
          <br />
          <hr />
          <br />
          <Grid item xs={12}>
              <center>
                  <Dropzone onDrop={handleDrop}>
                      {({ getRootProps, getInputProps }) => (
                          <div {...getRootProps({ className: "dropzone" })}>
                              <input accept="image/*" {...getInputProps()} />
                              <Button size="large" color="primary" style={{ marginTop: '10px' }} variant="contained">Upload Photo</Button>
                          </div>
                      )}
                  </Dropzone>
              </center>
          </Grid>

          <button type="button" className="btn btn-primary mt-5 mb-2 float-right" onClick={() => handleAdd()}>
              Add Size
          </button>
          {fields.map((field, idx) => {
              return (
              <div key={`${field}-${idx}`}>
                  <Grid item xs={12}>
                    <TextField
                        variant="outlined"
                        required
                        fullWidth
                        label="Size in gram"
                        onChange={e => handleChange(idx, e)}
                        className="mt-2 mb-2"
                    />
                  </Grid>
                  <button type="button" style={{ backgroundColor: 'red', color: '#fff' }} className="btn btn-delete mt-2 mb-2" onClick={() => handleRemove(idx)}>
                    Delete
                  </button>
                  <hr />
              </div>
              );
          })}


          <hr />
          <br />
          <button onClick={addFlower} className="btn btn-primary btn-block btn-lg">Submit</button>
        </Container>
      </main>
    </div>
  );
}

