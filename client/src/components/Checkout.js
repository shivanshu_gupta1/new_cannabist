import React from 'react';
// Components
import Navigation from './static/Navigation';
import Footer from './static/Footer';
// API Service
import { API_SERVICE } from '../config/URI';
import { firestore } from '../Firebase/index';
import Snackbar from '@material-ui/core/Snackbar';
import CloseIcon from '@material-ui/icons/Close';
import MuiAlert from '@material-ui/lab/Alert';
import IconButton from '@material-ui/core/IconButton';
import axios from 'axios';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';
// Stripe
import {Elements} from '@stripe/react-stripe-js';
import {loadStripe} from '@stripe/stripe-js';
import HomePage from './HomePage';

const stripePromise = loadStripe('pk_test_51IQmDSLbp71n4XnIqbqqCefhzj2vC0QfGa0CXHMcuF0XkfgBu5O8G8C3ArCAhTamuUISvENitKXwmCYB9CGdbTHo008s70cMF8');

const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
    },
    button: {
      marginRight: theme.spacing(1),
    },
    instructions: {
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(1),
    },
}));


function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const ProductList = ({ p, settotalprice, product }) => {
    var size = Number(product.size);
    var qtyProduct = Number(product.qty);
    var price = size * p;
    price = price * qtyProduct;
    settotalprice(price.toFixed(2));
    
    return (
        <>
            
        </>
    )
}



function getSteps() {
    return ['Information', 'Payment'];
}


const Checkout = ({ location }) => {
    const classes = useStyles();
    const [message, setmessage] = React.useState('');
    const [products, setProducts] = React.useState([]);
    const [loading, setLoading] = React.useState(true);
    const [totalprice, settotalprice] = React.useState('');
    const [open, setOpen] = React.useState(false);
    

    const handleClick = () => {
        setOpen(true);
    };
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
        return;
        }
        setOpen(false);
    };

    React.useEffect(() => {
        var uid = sessionStorage.getItem("userId");
        axios.get(`${API_SERVICE}/api/v1/main/findallthecartitems/${uid}`)
            .then(response => {
                setProducts(response.data);
                setLoading(false);
            })
    }, []);

    const refreshList = () => {
        var uid = sessionStorage.getItem("userId");
        axios.get(`${API_SERVICE}/api/v1/main/findallthecartitems/${uid}`)
            .then(response => {
                setProducts(response.data);
                setLoading(false);
            })
    }

    const deleteItem = (documentId) => {
        setLoading(true);
        var uid = sessionStorage.getItem("userId");
        var docRef = firestore.collection("cart").doc(uid);
        docRef.get().then(function(doc) {
            var items = doc.data().items;
            axios.get(`${API_SERVICE}/api/v1/main/removeitemtocart/${documentId}`)
                .then((response) => {
                    if (response.status === 200) {
                        refreshList();
                        handleClick();
                        setmessage('Item Removed from Cart');
                        items = items - 1;
                        docRef.set({
                            items 
                        }, { merge: true });
                    } 
                }).catch(err => console.log(err));
        }).catch(function(error) {
            console.log("Error getting document:", error);
        });
    }

    const showProductList = () => {
        var p = 0;
        return products.map(product => {
            p = Number(product.product.price) + Number(p);
            return <ProductList deleteItem={deleteItem} settotalprice={settotalprice} p={p} product={product} />
        })
    }

    const [activeStep, setActiveStep] = React.useState(0);
    const [skipped, setSkipped] = React.useState(new Set());
    const steps = getSteps();

    const isStepOptional = (step) => {
        return step === 1;
    };
    
    const isStepSkipped = (step) => {
        return skipped.has(step);
    };
    
    const handleNext = () => {
        let newSkipped = skipped;
        if (isStepSkipped(activeStep)) {
          newSkipped = new Set(newSkipped.values());
          newSkipped.delete(activeStep);
        }
    
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
        setSkipped(newSkipped);
    };
    
    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };
    
    const handleSkip = () => {
    if (!isStepOptional(activeStep)) {
      throw new Error("You can't skip a step that isn't optional.");
    }
    
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped((prevSkipped) => {
          const newSkipped = new Set(prevSkipped.values());
          newSkipped.add(activeStep);
          return newSkipped;
        });
    };
    
    const handleReset = () => {
        setActiveStep(0);
    };
    

    return (
        <>
            <Snackbar
                anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
                }}
                open={open}
                autoHideDuration={3000}
                onClose={handleClose}
                action={
                <React.Fragment>
                    <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
                    <CloseIcon fontSize="small" />
                    </IconButton>
                </React.Fragment>
                }
            >
                <Alert onClose={handleClose} severity="success">
                    {message}
                </Alert>
            </Snackbar>
            <Navigation />
            <div style={{ backgroundColor: 'gainsboro' }} className="jumbotron jumbotron-fluid">
                <div className="container text-center">
                    <h2 className="display-4 font-weight-bold">Checkout</h2>
                    <p className="lead font-weight-bold text-success h5">
                        {
                            products && products.length ? (
                            <>    
                                {`Total Price $ ${totalprice} CAD`}
                            </>
                            ) : (
                                `Total Price $ 0 CAD`
                            )
                        }
                    </p>
                </div>
            </div>
            <section className="m-4">
                {
                    loading === true ? (
                        <center style={{ marginTop: '10%' }}>
                            <CircularProgress />
                        </center>
                    ) : (
                        products && products.length ? (
                        <>
                        <Elements stripe={stripePromise}>
                            <HomePage type={'vg'} amount={totalprice} />
                        </Elements>
                        {showProductList()}
                        </>
                        ) : (
                            <>
                                <center style={{ marginTop: '5%' }}>
                                    <h1>
                                        Empty Cart
                                    </h1>
                                    <img alt="Empty Cart" alt="Cart" src="https://cdn.dribbble.com/users/2046015/screenshots/4591856/first_white_girl_drbl.gif" />
                                </center>
                            </>
                        )
                    )
                }
            </section>
            <Footer />
        </>
    )
}

export default Checkout
