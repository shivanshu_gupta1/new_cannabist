import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import {
    BrowserView,
    MobileView
} from "react-device-detect";

const days = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
const years = [1950, 1951, 1952, 1953, 1954, 1955, 1956, 1957, 1958, 1959, 1960, 1961, 1962, 1963, 1964, 1965, 1966, 1967, 1968, 1969, 1970, 1971, 1972, 1973, 1974, 1975, 1976, 1977, 1978, 1979, 1980, 1981, 1982, 1983, 1984, 1985, 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024];

const Verifyage = () => {
    const [open, setOpen] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };

    let [age, setage] = React.useState(0);

    let [day, setday] = React.useState('');
    let [month, setmonth] = React.useState('');


    const checkAge = () => {
        age = Number(age);
        if (day !== '' && month !== '') {
            if (age < 1998 && age !== 0) {
                window.location.href = "/home";
                setage("");
            } else if (age >= 1998) {
                handleClickOpen();
                setage("");
            }
        }
    }
    return (
        <>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle style={{ color: '#000000', fontSize: '2vh' }} id="alert-dialog-title">{"Age"}</DialogTitle>
                <DialogContent>
                <DialogContentText style={{ color: '#000000', fontSize: '3vh' }} id="alert-dialog-description">
                    Sorry, you must be 18+ to access this website.
                </DialogContentText>
                </DialogContent>
                <DialogActions>
                <Button onClick={handleClose} color="primary">
                    Close
                </Button>
                </DialogActions>
            </Dialog>
        
            <center>
                <img className="mt-2 mb-5" src="https://res.cloudinary.com/dx9dnqzaj/image/upload/v1614106249/cannabis/1j_ojFVDOMkX9Wytexe43D6khvaArh9OnhrNwXs1M3EMoAJtliEph.._twtjlx.png" />
            </center>
            <BrowserView>
                <center class="container w-50">
                    <h2 className="display-4 font-weight-bold mb-4">18+</h2>
                    <h2 className="display-4 font-weight-bold mb-4">Please tell us your age.</h2>
                    <h2 className="display-4 font-weight-bold mb-4">MM / DD / YYYY</h2>
                    <div className="row">
                        <div className="col-md">
                            <FormControl style={{ width: '100%' }}>
                            <InputLabel style={{ fontSize: '2vh', color: '#000000' }} id="demo-simple-select-label">Month</InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select-outlined"
                                fullWidth
                                style={{ fontSize: '2vh', color: '#000000' }} 
                                onChange={(e) => setmonth(e.target.value)}  
                            >
                            <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='01'>January</MenuItem>
                            <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='02'>February</MenuItem>
                            <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='03'>March</MenuItem>
                            <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='04'>April</MenuItem>
                            <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='05'>May</MenuItem>
                            <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='06'>June</MenuItem>
                            <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='07'>July</MenuItem>
                            <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='08'>August</MenuItem>
                            <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='09'>September</MenuItem>
                            <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='10'>October</MenuItem>
                            <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='11'>November</MenuItem>
                            <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='12'>December</MenuItem>
                            </Select>
                            </FormControl>
                        </div>
                        <div className="col-md">
                                <FormControl style={{ width: '100%' }}>
                                <InputLabel style={{ fontSize: '2vh', color: '#000000' }} id="demo-simple-select-label">Day</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select-outlined"
                                    fullWidth
                                    style={{ fontSize: '2vh', color: '#000000' }} 
                                    onChange={(e) => setday(e.target.value)}
                                >
                                {
                                    days.map((d) => (
                                        <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value={d}>{d}</MenuItem>
                                    ))
                                }
                            </Select>
                            </FormControl>
                        </div>
                        <div className="col-md">
                            <FormControl style={{ width: '100%' }}>
                                <InputLabel style={{ fontSize: '2vh', color: '#000000' }} id="demo-simple-select-label">Years</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select-outlined"
                                    fullWidth
                                    style={{ fontSize: '2vh', color: '#000000' }} 
                                    onChange={(e) => setage(e.target.value)}
                                >
                                {
                                    years.map((y) => (
                                        <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value={y}>{y}</MenuItem>
                                    ))
                                }
                            </Select>
                            </FormControl>
                        </div>
                    </div>
                    <br />
                    <button style={{ fontSize: '2vh' }} onClick={checkAge} class="btn btn-lg  btn-block btn-outline-success">
                        <b>Submit</b>
                    </button>
                </center>
            </BrowserView>

            <MobileView>
                <div className="">
                    <center>
                        <h2 className="display-4 font-weight-bold mb-4">18+</h2>
                        <h2 className="display-4 font-weight-bold mb-4">Please tell us your age.</h2>
                        <h2 className="display-4 font-weight-bold mb-4">MM / DD / YYYY</h2>
                    <div className="row container">
                        <div className="col-md">
                            <FormControl style={{ width: '100%' }}>
                            <InputLabel style={{ fontSize: '2vh', color: '#000000' }} id="demo-simple-select-label">Month</InputLabel>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select-outlined"
                                fullWidth
                                style={{ fontSize: '2vh', color: '#000000' }} 
                                onChange={(e) => setmonth(e.target.value)}  
                            >
                            <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='01'>January</MenuItem>
                            <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='02'>February</MenuItem>
                            <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='03'>March</MenuItem>
                            <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='04'>April</MenuItem>
                            <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='05'>May</MenuItem>
                            <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='06'>June</MenuItem>
                            <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='07'>July</MenuItem>
                            <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='08'>August</MenuItem>
                            <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='09'>September</MenuItem>
                            <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='10'>October</MenuItem>
                            <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='11'>November</MenuItem>
                            <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='12'>December</MenuItem>
                            </Select>
                            </FormControl>
                        </div>
                        <div className="col-md">
                                <FormControl style={{ width: '100%' }}>
                                <InputLabel style={{ fontSize: '2vh', color: '#000000' }} id="demo-simple-select-label">Day</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select-outlined"
                                    fullWidth
                                    style={{ fontSize: '2vh', color: '#000000' }} 
                                    onChange={(e) => setday(e.target.value)}
                                >
                                {
                                    days.map((d) => (
                                        <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value={d}>{d}</MenuItem>
                                    ))
                                }
                            </Select>
                            </FormControl>
                        </div>
                        <div className="col-md">
                            <FormControl style={{ width: '100%' }}>
                                <InputLabel style={{ fontSize: '2vh', color: '#000000' }} id="demo-simple-select-label">Years</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select-outlined"
                                    fullWidth
                                    style={{ fontSize: '2vh', color: '#000000' }} 
                                    onChange={(e) => setage(e.target.value)}
                                >
                                {
                                    years.map((y) => (
                                        <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value={y}>{y}</MenuItem>
                                    ))
                                }
                            </Select>
                            </FormControl>
                        </div>
                    </div>
                    <br />
                    <button style={{ fontSize: '2vh' }} onClick={checkAge} class="btn btn-lg w-50 btn-outline-success">
                        <b>Submit</b>
                    </button>
                    </center>
                </div>
            </MobileView>
            

            <section className="mt-5">
                <div class="container2">
                    <img className="w-100 img img-fluid" src="//cdn.shopify.com/s/files/1/2636/1928/files/Hero-Desktop_8ffcd371-cdbb-4f73-8316-c14492227a9b.jpg" />
                    <div className="centered">
                        <span  style={{ fontSize: '4vw', fontWeight: 'bold' }}>The best website for your favorites weeds</span> <br />
                        <span  style={{ fontSize: '3vw', fontWeight: 'bold' }}>
                        </span> <br />
                    </div>
                </div> 
            </section>
        </>
    )
}

export default Verifyage
