import React from 'react';
import Grid from '@material-ui/core/Grid';
// Components
import Navigation from './static/Navigation';
import Footer from './static/Footer';
// API Service
import { API_SERVICE, SECRET_KEY } from '../config/URI';
import { firestore } from '../Firebase/index';
import Snackbar from '@material-ui/core/Snackbar';
import CloseIcon from '@material-ui/icons/Close';
import MuiAlert from '@material-ui/lab/Alert';
import IconButton from '@material-ui/core/IconButton';
import axios from 'axios';
import CircularProgress from '@material-ui/core/CircularProgress';
import Box from '@material-ui/core/Box';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Typography from '@material-ui/core/Typography';
import { auth } from "../Firebase/index";

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function CircularProgressWithLabel(props) {
    return (
      <Box position="relative" display="inline-flex">
        <CircularProgress variant="determinate" {...props} />
        <Box
          top={0}
          left={0}
          bottom={0}
          right={0}
          position="absolute"
          display="flex"
          alignItems="center"
          justifyContent="center"
        >
          <Typography variant="h6" component="div" color="textSecondary">{`${Math.round(
            props.value,
          )}%`}</Typography>
        </Box>
      </Box>
    );
}

const ProductList = ({ product, removeItem, setsize, addItem }) => {
    var size = product.product.size;
    var thc = Number(product.product.thc);
    var cbd = Number(product.product.cbd);
    return (
            <div className="col-lg-3 col-md-5 mb-4">
                <div className="card card-ecommerce">
                    <center>
                        <img src={product.product.photoDownloadUrl1} style={{ width: '38vh', height: '36vh' }} className="img-fluid mt-2" alt="" />
                    </center>
                    <div className="card-body">
                    <center>
                        <h4>
                            ({product.product.company}) <br />
                            {product.product.name} <br />
                            {product.product.category} <br /> 
                            <strong className="text-center">{product.product.price} $</strong>
                        </h4>
                    </center>
                    <center>
                    <div className="row mt-1">
                        <div className="col-md">
                            <h5 style={{ fontWeight: 'bold' }} >THC</h5>
                            {
                                thc >= 26 ? (
                                    <CircularProgressWithLabel style={{ color: 'red' }} value={thc} />
                                ) : thc < 26 && thc >= 10 ? (
                                    <CircularProgressWithLabel style={{ color: 'orange' }} value={thc} />
                                ) : (
                                    <CircularProgressWithLabel style={{ color: 'blue' }} value={thc} />
                                )
                            }
                        </div>
                        <div className="col-md">
                            <h5 style={{ fontWeight: 'bold' }} >CBD</h5>
                            {
                                cbd >= 26 ? (
                                    <CircularProgressWithLabel style={{ color: 'red' }} value={cbd} />
                                ) : cbd < 26 && cbd >= 10 ? (
                                    <CircularProgressWithLabel style={{ color: 'orange' }} value={cbd} />
                                ) : (
                                    <CircularProgressWithLabel style={{ color: 'blue' }} value={cbd} />
                                )
                            }
                        </div>
                    </div>
                    </center>
                    <FormControl style={{ width: '100%' }}>
                    <InputLabel style={{ fontSize: '2vh', color: '#000000' }} id="demo-simple-select-label">Select Size</InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select-outlined"
                        fullWidth
                        style={{ fontSize: '2vh', color: '#000000' }} 
                        onChange={(event) => setsize(event.target.value)}
                    >
                    {
                        size.map((s) => (
                            <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value={s.size}>{s.size} g</MenuItem>
                        ))
                    }
                    </Select>
                    </FormControl>
                    <br />
                    <button onClick={() => addItem(product)} class="btn btn-block mt-2 btn-outline-success" style={{ fontSize: '2vh', fontWeight: 'bold'}}>Add to Cart</button>
                    <button onClick={() => removeItem(product._id)} style={{ fontSize: '2vh', border: 'none', fontWeight: 'bold'}} className="btn btn-block btn-lg mt-2 btn-danger">
                        Remove from Favorite
                    </button>
                    </div>
                </div>
            </div>
    )
}


const FavoriteItem = ({ location }) => {
    const [message, setMessage] = React.useState('');
    const [products, setProducts] = React.useState([]);
    const [loading, setLoading] = React.useState(true);
    const [totalprice, settotalprice] = React.useState(0);
    const [open, setOpen] = React.useState(false);
    const [size, setsize] = React.useState('');

    const handleClick = () => {
        setOpen(true);
    };
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
        return;
        }
        setOpen(false);
    };

    React.useEffect(() => {
        var userEmail = sessionStorage.getItem("userEmail");
        axios.get(`${API_SERVICE}/api/v1/main/findallfavorites/${userEmail}`)
            .then(response => {
                setProducts(response.data);
                setLoading(false);
            })
    }, []);

    const refreshList = () => {
        var userEmail = sessionStorage.getItem("userEmail");
        axios.get(`${API_SERVICE}/api/v1/main/findallfavorites/${userEmail}`)
            .then(response => {
                setProducts(response.data);
                setLoading(false);
            })
    }

    const removeItem = (documentId) => {
        setLoading(true);
        axios.get(`${API_SERVICE}/api/v1/main/removefavorite/${documentId}`)
            .then((response) => {
                if (response.status === 200) {
                    refreshList();
                } 
            }).catch(err => console.log(err));
    }

    const addItem = (product) => {
        auth.onAuthStateChanged(function(user) {
        if (!user) {

        } else {
            if (size === "") {

            } else {
                var uid = sessionStorage.getItem("userId");
                var docRef = firestore.collection("cart").doc(uid);
                docRef.get().then(function(doc) {
                    if (doc.exists) {
                        var items = doc.data().items;
                        if (items === 10000) {
                            handleClick();
                            setMessage('Maximum Items in Cart Exceeded');
                        } else {
                            // Send Item to the Card in Database
                            var uploadData = {
                                productId: product._id,
                                userId: uid,
                                size: size,
                                product: product.product
                            }
                            axios.post(`${API_SERVICE}/api/v1/main/additemtocart`, uploadData)
                                .then((response) => {
                                    if (response.status === 200) {
                                        handleClick();
                                        setMessage('Item Added to Cart');
                                        items = items + 1;
                                        docRef.set({
                                            items 
                                        }, { merge: true });
                                    } else if (response.status === 201) {
                                        handleClick();
                                        setMessage('Item Already Added to Cart');
                                    }
                                }).catch(err => console.log(err));
                        }
                        setsize("");
                    } else {
                        docRef.set({
                            items: 1 
                        }, { merge: true });
                        var uploadData = {
                            productId: product._id,
                            userId: uid,
                            size: size,
                            product
                        }
                        axios.post(`${API_SERVICE}/api/v1/main/additemtocart`, uploadData)
                            .then((response) => {
                                if (response.status === 200) {
                                    handleClick();
                                    setMessage('Item Added to Cart');
                                } 
                            }).catch(err => console.log(err));
                        setsize("");
                    }
                }).catch(function(error) {
                    console.log("Error getting document:", error);
                });
            }
        }
        });
    }

    const showProductList = () => {
        return products.map(product => {
            return <ProductList setsize={setsize} addItem={addItem} removeItem={removeItem} product={product} />
        })
    }
    

    return (
        <>
            <Snackbar
                anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
                }}
                open={open}
                autoHideDuration={3000}
                onClose={handleClose}
                action={
                <React.Fragment>
                    <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
                    <CloseIcon fontSize="small" />
                    </IconButton>
                </React.Fragment>
                }
            >
                <Alert onClose={handleClose} severity="success">
                    <h4 style={{ color: '#fff' }}>{message}</h4>
                </Alert>
            </Snackbar>
            <Navigation />
            <div style={{ backgroundColor: 'gainsboro' }} className="jumbotron jumbotron-fluid">
                <div className="container text-center">
                    <h2 className="display-4 font-weight-bold">Favorites</h2>
                </div>
            </div>
            <section className="m-4">
                {
                    loading === true ? (
                        <center style={{ marginTop: '10%' }}>
                            <CircularProgress />
                        </center>
                    ) : (
                        products && products.length ? (
                        <>
                            <div className="row">
                                {showProductList()}
                            </div>
                            <br />
                        </>
                        ) : (
                            <>
                                <center style={{ marginTop: '5%' }}>
                                    <h1>
                                        No Favorites Yet!
                                    </h1>
                                </center>
                            </>
                        )
                    )
                }
            </section>
            <Footer />
        </>
    )
}

export default FavoriteItem
