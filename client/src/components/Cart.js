import React from 'react';
// Components
import Navigation from './static/Navigation';
import Footer from './static/Footer';
// API Service
import { API_SERVICE, SECRET_KEY } from '../config/URI';
import { firestore } from '../Firebase/index';
import Snackbar from '@material-ui/core/Snackbar';
import CloseIcon from '@material-ui/icons/Close';
import MuiAlert from '@material-ui/lab/Alert';
import IconButton from '@material-ui/core/IconButton';
import axios from 'axios';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import {
    BrowserView,
    MobileView
} from "react-device-detect";

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function CircularProgressWithLabel(props) {
    return (
      <Box position="relative" display="inline-flex">
        <CircularProgress variant="determinate" {...props} />
        <Box
          top={0}
          left={0}
          bottom={0}
          right={0}
          position="absolute"
          display="flex"
          alignItems="center"
          justifyContent="center"
        >
          <Typography variant="h6" component="div" color="textSecondary">{`${Math.round(
            props.value,
          )}%`}</Typography>
        </Box>
      </Box>
    );
}

const ProductList = ({ deleteItem, changeQty, p, settotalprice, product }) => {
    var thc = Number(product.product.thc);
    var cbd = Number(product.product.cbd);
    // Price Calculating
    var size = Number(product.size);
    var qtyProduct = Number(product.qty);
    var price = size * p;
    price = price * qtyProduct;
    settotalprice(price.toFixed(2));

    return (
        <div className="col-lg-3 col-md-5 mb-4">
            <div className="card card-ecommerce">
                <center>
                    <img src={product.product.photoDownloadUrl1} style={{ width: '38vh', height: '36vh' }} className="img-fluid mt-2" alt="" />
                </center>
                <div className="card-body">
                <center>
                    <h4>
                        ({product.product.company}) <br />
                        {product.product.name} <br />
                        {product.product.category} <br /> 
                        {product.size}g<br />
                        <strong className="text-center">{product.product.price} $</strong>
                    </h4>
                </center>
                <center>
                <div className="row mt-1">
                    <div className="col-md">
                        <h5 style={{ fontWeight: 'bold' }} >THC</h5>
                        {
                            thc >= 26 ? (
                                <CircularProgressWithLabel style={{ color: 'red' }} value={thc} />
                            ) : thc < 26 && thc >= 10 ? (
                                <CircularProgressWithLabel style={{ color: 'orange' }} value={thc} />
                            ) : (
                                <CircularProgressWithLabel style={{ color: 'blue' }} value={thc} />
                            )
                        }
                    </div>
                    <div className="col-md">
                        <h5 style={{ fontWeight: 'bold' }} >CBD</h5>
                        {
                            cbd >= 26 ? (
                                <CircularProgressWithLabel style={{ color: 'red' }} value={cbd} />
                            ) : cbd < 26 && cbd >= 10 ? (
                                <CircularProgressWithLabel style={{ color: 'orange' }} value={cbd} />
                            ) : (
                                <CircularProgressWithLabel style={{ color: 'blue' }} value={cbd} />
                            )
                        }
                    </div>
                </div>
                </center>
                <FormControl style={{ width: '100%' }}>
                    <InputLabel style={{ fontSize: '2vh', color: '#000000' }} id="demo-simple-select-label">QTY</InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select-outlined"
                        fullWidth
                        style={{ fontSize: '2vh', color: '#000000' }} 
                        value={product.qty}
                        onChange={(e) => changeQty(product._id, e.target.value)}
                    >
                    <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='1'>1</MenuItem>
                    <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='2'>2</MenuItem>
                    <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='3'>3</MenuItem>
                    <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='4'>4</MenuItem>
                    <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='5'>5</MenuItem>
                    <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='6'>6</MenuItem>
                    <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='7'>7</MenuItem>
                    <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='8'>8</MenuItem>
                    <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='9'>9</MenuItem>
                    <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='10'>10</MenuItem>
                    </Select>
                </FormControl>
                <br />
                <br />
                <button onClick={() => deleteItem(product._id)} style={{ fontSize: '2vh', border: 'none', fontWeight: 'bold'}} className="btn btn-block btn-lg mt-2 btn-danger">
                    Remove the Item
                </button>
                </div>
            </div>
        </div>
    )
}


const ProductList2 = ({ deleteItem, changeQty, p, settotalprice, product }) => {
    var thc = Number(product.product.thc);
    var cbd = Number(product.product.cbd);
    // Price Calculating
    var size = Number(product.size);
    var qtyProduct = Number(product.qty);
    var price = size * p;
    price = price * qtyProduct;
    settotalprice(price.toFixed(2));

    return (
            <tr>
                <th scope="row">
                  <img src={product.product.photoDownloadUrl1} alt="" class="img-fluid z-depth-0" />
                </th>
                <td>
                  <h4 style={{ fontSize: '2vh', color: '#000000' }} class="mt-3">
                    <strong>{product.product.name}</strong> <br />
                  </h4>
                  <p style={{ fontSize: '12px', color: '#000000' }} >({product.product.category})</p>
                </td>
                <td style={{ fontSize: '2vh', color: '#000000' }}>
                THC: {thc} % <br /> CBD: {cbd} %
                </td>
                <td></td>
                <td style={{ fontSize: '2vh', color: '#000000' }}>
                {product.size}
                </td>
                <td class="text-center text-md-left">
                    <FormControl style={{ width: '100%' }}>
                    <InputLabel style={{ fontSize: '2vh', color: '#000000' }} id="demo-simple-select-label">QTY</InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select-outlined"
                        fullWidth
                        style={{ fontSize: '2vh', color: '#000000' }} 
                        value={product.qty}
                        onChange={(e) => changeQty(product._id, e.target.value)}
                    >
                    <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='1'>1</MenuItem>
                    <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='2'>2</MenuItem>
                    <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='3'>3</MenuItem>
                    <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='4'>4</MenuItem>
                    <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='5'>5</MenuItem>
                    <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='6'>6</MenuItem>
                    <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='7'>7</MenuItem>
                    <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='8'>8</MenuItem>
                    <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='9'>9</MenuItem>
                    <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value='10'>10</MenuItem>
                    </Select>
                    </FormControl>
                </td>
                <td style={{ fontSize: '2vh', color: '#000000' }} class="font-weight-bold">
                  <strong>{product.product.price} $</strong>
                </td>
                <td>
                  <button style={{ fontSize: '12px'}} onClick={() => deleteItem(product._id)} type="button" class="btn btn-sm btn-danger bnt-lg" data-toggle="tooltip" data-placement="top" title="" data-original-title="Remove item">
                    Remove Item
                  </button>
                </td>
              </tr>
    )
}


const Products = ({ location }) => {
    const [message, setmessage] = React.useState('');
    const [products, setProducts] = React.useState([]);
    const [loading, setLoading] = React.useState(true);
    const [totalprice, settotalprice] = React.useState(0);
    const [open, setOpen] = React.useState(false);

    const handleClick = () => {
        setOpen(true);
    };
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
        return;
        }
        setOpen(false);
    };

    React.useEffect(() => {
        var uid = sessionStorage.getItem("userId");
        axios.get(`${API_SERVICE}/api/v1/main/findallthecartitems/${uid}`)
            .then(response => {
                setProducts(response.data);
                setLoading(false);
            })
    }, []);

    const refreshList = () => {
        var uid = sessionStorage.getItem("userId");
        axios.get(`${API_SERVICE}/api/v1/main/findallthecartitems/${uid}`)
            .then(response => {
                setProducts(response.data);
                setLoading(false);
            })
    }

    const deleteItem = (documentId) => {
        setLoading(true);
        var uid = sessionStorage.getItem("userId");
        var docRef = firestore.collection("cart").doc(uid);
        docRef.get().then(function(doc) {
            var items = doc.data().items;
            axios.get(`${API_SERVICE}/api/v1/main/removeitemtocart/${documentId}`)
                .then((response) => {
                    if (response.status === 200) {
                        refreshList();
                        handleClick();
                        setmessage('Item Removed from Cart');
                        items = items - 1;
                        docRef.set({
                            items 
                        }, { merge: true });
                    } 
                }).catch(err => console.log(err));
        }).catch(function(error) {
            console.log("Error getting document:", error);
        });
    }

    const changeQty = (documentId, qty) => {
        setLoading(true);
        axios.get(`${API_SERVICE}/api/v1/main/cartitemchangequantitycart/${documentId}/${qty}`)
            .then((response) => {
                if (response.status === 200) {
                    refreshList();
                    handleClick();
                    setmessage('Qty Changed');
                } 
            }).catch(err => console.log(err));
    }

    const showProductList = () => {
        var p = 0;
        return products.map(product => {
            p = Number(product.product.price) + Number(p);
            return <ProductList changeQty={changeQty} deleteItem={deleteItem} settotalprice={settotalprice} p={p} product={product} />
        })
    }

    const showProductList2 = () => {
        var p = 0;
        return products.map(product => {
            p = Number(product.product.price) + Number(p);
            return <ProductList2 changeQty={changeQty} deleteItem={deleteItem} settotalprice={settotalprice} p={p} product={product} />
        })
    }
    

    return (
        <>
            <Snackbar
                anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
                }}
                open={open}
                autoHideDuration={3000}
                onClose={handleClose}
                action={
                <React.Fragment>
                    <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
                    <CloseIcon fontSize="small" />
                    </IconButton>
                </React.Fragment>
                }
            >
                <Alert onClose={handleClose} severity="success">
                    <h4 style={{ color: '#fff' }}>{message}</h4>
                </Alert>
            </Snackbar>
            <Navigation />
            <div style={{ backgroundColor: 'gainsboro' }} className="jumbotron jumbotron-fluid">
                <div className="container text-center">
                    <h2 className="display-4 font-weight-bold">Cart</h2>
                    <p className="lead font-weight-bold text-success h5">
                        {
                            products && products.length ? (
                            <>    
                                {`Total Price $ ${totalprice} CAD`}
                                <br />
                                <a href="/checkout" style={{ fontSize: '2vh' }} className="btn btn-lg btn-outline-success">
                                    Proceed to Checkout
                                </a>
                            </>
                            ) : (
                                `Total Price $ 0 CAD`
                            )
                        }
                    </p>
                </div>
            </div>
            <section className="m-4">
                <MobileView>
                {
                    loading === true ? (
                        <center style={{ marginTop: '10%' }}>
                            <CircularProgress />
                        </center>
                    ) : (
                        products && products.length ? (
                        <>
                            <div className="row">
                                {showProductList()}
                            </div>
                            <br />
                            <center>
                                <a href="/checkout" style={{ fontSize: '2vh' }} className="btn btn-lg btn-lg btn-success">
                                    Proceed to Checkout
                                </a>
                            </center>
                        </>
                        ) : (
                            <>
                                <center style={{ marginTop: '5%' }}>
                                    <h1>
                                        Empty Cart
                                    </h1>
                                    <img alt="Empty Cart" alt="Cart" src="https://cdn.dribbble.com/users/2046015/screenshots/4591856/first_white_girl_drbl.gif" />
                                </center>
                            </>
                        )
                    )
                }
                </MobileView>

                <BrowserView>
                {
                    loading === true ? (
                        <center style={{ marginTop: '10%' }}>
                            <CircularProgress />
                        </center>
                    ) : (
                        products && products.length ? (
                        <>
                            <div className="container">
                                <section className="section my-5 pb-5">
                                    <div className="table-responsive">
                                        <table className="table product-table">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th style={{ fontSize: '2vh', color: '#000000', width: '16%' }} class="font-weight-bold">
                                                    <strong>Product</strong>
                                                    </th>
                                                    <th style={{ fontSize: '2vh', color: '#000000', width: '12%' }} class="font-weight-bold">
                                                    <strong>THC/CBD</strong>
                                                    </th>
                                                    <th style={{ fontSize: '2vh', color: '#000000', width: '6%' }} ></th>
                                                    <th style={{ fontSize: '2vh', color: '#000000', width: '10%' }} class="font-weight-bold">
                                                    <strong>Size</strong>
                                                    </th>
                                                    <th style={{ fontSize: '2vh', color: '#000000', width: '10%'  }} class="font-weight-bold">
                                                    <strong>QTY</strong>
                                                    </th>
                                                    <th style={{ fontSize: '2vh', color: '#000000', width: '12%' }} class="font-weight-bold">
                                                    <strong>Amount</strong>
                                                    </th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {showProductList2()}
                                                <tr>
                                                    <td colspan="3"></td>
                                                    <td colspan="3" class="text-right">
                                                    </td>
                                                    <td>
                                                    <h4 class="mt-2">
                                                        <strong>HST (13%)</strong>
                                                    </h4>
                                                    </td>
                                                    <td class="text-right">
                                                    <h4 class="mt-2">
                                                        <strong>TAXES INCLUDED</strong>
                                                    </h4>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3"></td>
                                                    <td colspan="3" class="text-right">
                                                    </td>
                                                    <td>
                                                    <h4 class="mt-2">
                                                        <strong>Total</strong>
                                                    </h4>
                                                    </td>
                                                    <td class="text-right">
                                                    <h4 class="mt-2">
                                                        <strong>$ {totalprice}</strong>
                                                    </h4>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </section>
                            </div>
                            <br />

                            <h2>

                            </h2>
                            <center>
                                <a href="/checkout" style={{ fontSize: '2vh' }} className="btn btn-lg btn-lg btn-success">
                                    Proceed to Checkout
                                </a>
                            </center>
                        </>
                        ) : (
                            <>
                                <center style={{ marginTop: '5%' }}>
                                    <h1>
                                        Empty Cart
                                    </h1>
                                    <img alt="Empty Cart" alt="Cart" src="https://cdn.dribbble.com/users/2046015/screenshots/4591856/first_white_girl_drbl.gif" />
                                </center>
                            </>
                        )
                    )
                }
                </BrowserView>
            </section>
            <Footer />
        </>
    )
}

export default Products
