import React from 'react';
// Components
import Navigation from './static/Navigation';
import Footer from './static/Footer';
// API Service
import { API_SERVICE } from '../config/URI';
import axios from 'axios';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

function CircularProgressWithLabel(props) {
    return (
      <Box position="relative" display="inline-flex">
        <CircularProgress variant="determinate" {...props} />
        <Box
          top={0}
          left={0}
          bottom={0}
          right={0}
          position="absolute"
          display="flex"
          alignItems="center"
          justifyContent="center"
        >
          <Typography variant="h6" component="div" color="textSecondary">{`${Math.round(
            props.value,
          )}%`}</Typography>
        </Box>
      </Box>
    );
}

const ProductList = ({ p, settotalprice, product }) => {
    settotalprice(p.toFixed(2));
    var thc = Number(product.product.thc);
    var cbd = Number(product.product.cbd);
    var size = product.size;

    return (
        <div className="col-lg-3 col-md-5 mb-4">
            <div className="card card-ecommerce">
                <center>
                    <img src={product.product.photoDownloadUrl1} style={{ width: '38vh', height: '36vh' }} className="img-fluid mt-2" alt="" />
                </center>
                <div className="card-body">
                <center>
                    <h4>
                        ({product.product.company}) <br />
                        {product.product.name} <br />
                        {product.product.category} <br /> 
                        {product.size}g<br />
                        <strong className="text-center">{product.product.price} $</strong>
                    </h4>
                </center>
                <center>
                <div className="row mt-1">
                    <div className="col-md">
                        <h5 style={{ fontWeight: 'bold' }} >THC</h5>
                        {
                            thc >= 26 ? (
                                <CircularProgressWithLabel style={{ color: 'red' }} value={thc} />
                            ) : thc < 26 && thc >= 10 ? (
                                <CircularProgressWithLabel style={{ color: 'orange' }} value={thc} />
                            ) : (
                                <CircularProgressWithLabel style={{ color: 'blue' }} value={thc} />
                            )
                        }
                    </div>
                    <div className="col-md">
                        <h5 style={{ fontWeight: 'bold' }} >CBD</h5>
                        {
                            cbd >= 26 ? (
                                <CircularProgressWithLabel style={{ color: 'red' }} value={cbd} />
                            ) : cbd < 26 && cbd >= 10 ? (
                                <CircularProgressWithLabel style={{ color: 'orange' }} value={cbd} />
                            ) : (
                                <CircularProgressWithLabel style={{ color: 'blue' }} value={cbd} />
                            )
                        }
                    </div>
                </div>
                </center>
                </div>
            </div>
        </div>
    )
}


const Products = ({ location }) => {
    const [message, setmessage] = React.useState('');
    const [products, setProducts] = React.useState([]);
    const [loading, setLoading] = React.useState(true);
    const [totalprice, settotalprice] = React.useState(0);
    const [open, setOpen] = React.useState(false);

    React.useEffect(() => {
        var uid = sessionStorage.getItem("userId");
        axios.get(`${API_SERVICE}/api/v1/main/trackorder/${uid}`)
            .then(response => {
                setProducts(response.data);
                setLoading(false);
            })
    }, []);

    const showProductList = () => {
        var p = 0;
        return products.map(product => {
            p = Number(product.product.price) + Number(p);
            return <ProductList settotalprice={settotalprice} p={p} product={product} />
        })
    }

    return (
        <>
            <Navigation />
            <div style={{ backgroundColor: 'gainsboro' }} className="jumbotron jumbotron-fluid">
                <div className="container text-center">
                    <h2 className="display-4 font-weight-bold">Track Order</h2>
                </div>
            </div>
            <section className="m-4 mt-5 mb-5">
                <h2>Orders</h2>
                <br />
                {
                    loading === true ? (
                        <center style={{ marginTop: '10%' }}>
                            <CircularProgress />
                        </center>
                    ) : (
                        products && products.length ? (
                        <>
                            <h2>Total Price ${totalprice} CAD <b className="text-success">PAID</b></h2>
                            <div className="row">
                                {showProductList()}
                            </div>
                            <br />
                        </>
                        ) : (
                            <>
                                <center style={{ marginTop: '5%' }}>
                                    <h1>
                                        No Orders Found
                                    </h1>
                                    <img alt="Empty Cart" alt="Cart" src="https://cdn.dribbble.com/users/2046015/screenshots/4591856/first_white_girl_drbl.gif" />
                                </center>
                            </>
                        )
                    )
                }
            </section>
            <Footer />
        </>
    )
}

export default Products
