import React from 'react';
// Components
import Navigation from './static/Navigation';
import Footer from './static/Footer';
// API Service
import { API_SERVICE, SECRET_KEY } from '../config/URI';
import Snackbar from '@material-ui/core/Snackbar';
import CloseIcon from '@material-ui/icons/Close';
import MuiAlert from '@material-ui/lab/Alert';
import IconButton from '@material-ui/core/IconButton';
import axios from 'axios';
import { firestore, googleProvider, facebookProvider, auth } from "../Firebase/index";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import { withStyles } from '@material-ui/core/styles';
import Slider from '@material-ui/core/Slider';
import Favorite from './static/Favorite';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import {
    BrowserView,
    MobileView
} from "react-device-detect";
import clsx from 'clsx';
import FilterListIcon from '@material-ui/icons/FilterList';
import Dock from 'react-dock';

const PrettoSlider = withStyles({
    root: {
      color: '#52af77',
      height: 8,
    },
    thumb: {
      height: 24,
      width: 24,
      backgroundColor: '#fff',
      border: '2px solid currentColor',
      marginTop: -8,
      marginLeft: -12,
      '&:focus, &:hover, &$active': {
        boxShadow: 'inherit',
      },
    },
    active: {},
    valueLabel: {
      left: 'calc(-50% + 4px)',
    },
    track: {
      height: 8,
      borderRadius: 4,
    },
    rail: {
      height: 8,
      borderRadius: 4,
    },
})(Slider);


function CircularProgressWithLabel(props) {
    return (
      <Box position="relative" display="inline-flex">
        <CircularProgress variant="determinate" {...props} />
        <Box
          top={0}
          left={0}
          bottom={0}
          right={0}
          position="absolute"
          display="flex"
          alignItems="center"
          justifyContent="center"
        >
          <Typography variant="h6" component="div" color="textSecondary">{`${Math.round(
            props.value,
          )}mg`}</Typography>
        </Box>
      </Box>
    );
}

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles({
    list: {
      width: 250,
    },
    fullList: {
      width: 'auto',
    },
});

function valuetext(value) {
    return `${value}`;
}

const ProductList = ({ product, setsize, addItem }) => {
    var size = product.size;
    var thc = Number(product.thc);
    var cbd = Number(product.cbd);
    return (
            <div className="col-lg-4 col-md-6 mb-4">
                <div className="card card-ecommerce">
                    <center>
                        <img src={product.photoDownloadUrl1} style={{ width: '38vh', height: '36vh' }} className="img-fluid mt-2" alt="" />
                    </center>
                    <div className="card-body">
                    <center>
                        <h4>
                            ({product.company}) <br />
                            {product.name} <br />
                            {product.category} <br /> 
                            ({product.subcategory}) <br />
                            <strong className="text-center">{product.price} $</strong>
                        </h4>
                    </center>
                    <center>
                    <div className="row mt-1">
                        <div className="col-md">
                            <h5 style={{ fontWeight: 'bold' }} >THC</h5>
                            {
                                thc >= 20 ? (
                                    <CircularProgressWithLabel style={{ color: 'red' }} value={product.thc} />
                                ) : thc < 20 && thc >= 10 ? (
                                    <CircularProgressWithLabel style={{ color: 'orange' }} value={product.thc} />
                                ) : (
                                    <CircularProgressWithLabel style={{ color: 'blue' }} value={product.thc} />
                                )
                            }
                        </div>
                        <div className="col-md">
                            <h5 style={{ fontWeight: 'bold' }} >CBD</h5>
                            {
                                cbd >= 20 ? (
                                    <CircularProgressWithLabel style={{ color: 'red' }} value={product.thc} />
                                ) : cbd < 20 && cbd >= 10 ? (
                                    <CircularProgressWithLabel style={{ color: 'orange' }} value={product.thc} />
                                ) : (
                                    <CircularProgressWithLabel style={{ color: 'blue' }} value={product.thc} />
                                )
                            }
                        </div>
                    </div>
                    </center>
                    <FormControl style={{ width: '100%' }}>
                    <InputLabel style={{ fontSize: '2vh', color: '#000000' }} id="demo-simple-select-label">Select Size</InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select-outlined"
                        fullWidth
                        style={{ fontSize: '2vh', color: '#000000' }} 
                        onChange={(event) => setsize(event.target.value)}
                    >
                    {
                        size.map((s) => (
                            <MenuItem style={{ fontSize: '2vh', color: '#000000' }} value={s.size}>{s.size} g</MenuItem>
                        ))
                    }
                    </Select>
                    </FormControl>
                    <br />
                    <button onClick={() => addItem(product)} class="btn btn-block mt-2 btn-outline-success" style={{ fontSize: '2vh', fontWeight: 'bold'}}>Add to Cart</button>
                    <Favorite product={product} />
                    </div>
                </div>
            </div>
    )
}



const ProductList2 = ({ deleteItem, p, settotalprice, product }) => {
    settotalprice(p.toFixed(2));
    var thc = Number(product.product.thc);
    var cbd = Number(product.product.cbd);
    var size = product.size;

    return (
        <div className="card mt-2">
            <center>
                <img src={product.product.photoDownloadUrl1} style={{ width: '22vh', height: '22vh' }} className="img-fluid mt-2" alt="" />
            </center>
            <div className="card-body">
            <center>
                <h4>
                    ({product.product.company}) <br />
                    {product.product.name} <br />
                    {product.product.category} <br /> 
                    {product.size}g<br />
                    <strong className="text-center">{product.product.price} $</strong>
                </h4>
            </center>
            <center>
            <div className="row mt-1">
                <div className="col-md">
                    <h5 style={{ fontWeight: 'bold' }} >THC</h5>
                    {
                        thc >= 26 ? (
                            <CircularProgressWithLabel style={{ color: 'red' }} value={thc} />
                        ) : thc < 26 && thc >= 10 ? (
                            <CircularProgressWithLabel style={{ color: 'orange' }} value={thc} />
                        ) : (
                            <CircularProgressWithLabel style={{ color: 'blue' }} value={thc} />
                        )
                    }
                </div>
                <div className="col-md">
                    <h5 style={{ fontWeight: 'bold' }} >CBD</h5>
                    {
                        cbd >= 26 ? (
                            <CircularProgressWithLabel style={{ color: 'red' }} value={cbd} />
                        ) : cbd < 26 && cbd >= 10 ? (
                            <CircularProgressWithLabel style={{ color: 'orange' }} value={cbd} />
                        ) : (
                            <CircularProgressWithLabel style={{ color: 'blue' }} value={cbd} />
                        )
                    }
                </div>
            </div>
            </center>
            <br />
            <button onClick={() => deleteItem(product._id)} style={{ fontSize: '2vh', border: 'none', fontWeight: 'bold'}} className="btn btn-block btn-lg mt-2 btn-danger">
                Remove the Item
            </button>
            </div>
        </div>
    )
}


const Edibles = ({ location }) => {
    const classes = useStyles();
    const [query, setquery] = React.useState('');
    const [heading, setheading] = React.useState('');
    const [subheading, setsubheading] = React.useState('');
    const [totalItems, setTotalItems] = React.useState(0);
    const [expanded, setExpanded] = React.useState(false);
    const [value, setValue] = React.useState([0, 37]);
    const [message, setMessage] = React.useState('');
    const [open, setOpen] = React.useState(false);
    const [filter, setfilter] = React.useState('');
    const [size, setsize] = React.useState('');
    const [user, setUser] = React.useState({});
    const [userlogin, setUserLogin] = React.useState();
    const [openLogin, setOpenLogin] = React.useState(false);
    const [openVerify, setOpenVerify] = React.useState(false);
    const [fullName, setFullName] = React.useState('');
    const [email, setEmail] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [openSnackbar, setOpenSnackbar] = React.useState(false);
    const [valuethc, setValuethc] = React.useState([0, 100]);
    const [products, setproducts] = React.useState([]);
    const [loading, setloading] = React.useState(true);
    const [opencartdrawel, setopencartdrawel] = React.useState(false);
    const [products2, setProducts2] = React.useState([]);
    const [totalprice, settotalprice] = React.useState(0);
    const [loading2, setLoading2] = React.useState(true);

    React.useEffect(() => {
        var uid = sessionStorage.getItem("userId");
        axios.get(`${API_SERVICE}/api/v1/main/findallthecartitems/${uid}`)
            .then(response => {
                setProducts2(response.data);
                setLoading2(false);
            })
    }, []);

    const refreshList = () => {
        var uid = sessionStorage.getItem("userId");
        axios.get(`${API_SERVICE}/api/v1/main/findallthecartitems/${uid}`)
            .then(response => {
                setProducts2(response.data);
                setLoading2(false);
            })
    }

    const deleteItem = (documentId) => {
        setLoading2(true);
        var uid = sessionStorage.getItem("userId");
        var docRef = firestore.collection("cart").doc(uid);
        docRef.get().then(function(doc) {
            var items = doc.data().items;
            axios.get(`${API_SERVICE}/api/v1/main/removeitemtocart/${documentId}`)
                .then((response) => {
                    if (response.status === 200) {
                        refreshList();
                        items = items - 1;
                        docRef.set({
                            items 
                        }, { merge: true });
                    } 
                }).catch(err => console.log(err));
        }).catch(function(error) {
            console.log("Error getting document:", error);
        });
    }

    const showProductList2 = () => {
        var p = 0;
        return products2.map(product => {
            p = Number(product.product.price) + Number(p);
            return <ProductList2 deleteItem={deleteItem} settotalprice={settotalprice} p={p} product={product} />
        })
    }

    const [state, setState] = React.useState({
        left: false,
    });

    const toggleDrawer = (anchor, open) => (event) => {
        if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
          return;
        }
        setState({ ...state, [anchor]: open });
    };

     // Filters

     const filterSize = (size) => {
        setloading(true);
        axios.get(`${API_SERVICE}/api/v1/main/getediblesfiltersize/${size}`)
            .then(response => {
                setproducts(response.data);
                setloading(false);
            })
            .catch(err => console.log(err))
    }

    
    const filterCategory = (category) => {
        setloading(true);
        axios.get(`${API_SERVICE}/api/v1/main/getediblesfiltercategory/${category}`)
            .then(response => {
                setproducts(response.data);
                setloading(false);
            })
            .catch(err => console.log(err))
    }

    const filterSubCategory = (subcategory) => {
        setloading(true);
        axios.get(`${API_SERVICE}/api/v1/main/getediblesfiltersubcategory/${subcategory}`)
            .then(response => {
                setproducts(response.data);
                setloading(false);
            })
            .catch(err => console.log(err))
    }

    const filterthc = (event, newValue) => {
        setloading(true);
        axios.get(`${API_SERVICE}/api/v1/main/getediblesfilterthc/${newValue}`)
            .then(response => {
                setproducts(response.data);
                setloading(false);
            })
            .catch(err => console.log(err))
    }

    const filtercbd = (event, newValue) => {
        setloading(true);
        axios.get(`${API_SERVICE}/api/v1/main/getediblesfiltercbd/${newValue}`)
            .then(response => {
                setproducts(response.data);
                setloading(false);
            })
            .catch(err => console.log(err))
    }

    const handleChangethc = (event, newValue) => {
        setValuethc(newValue);
    };

    const [valuecbd, setValuecbd] = React.useState([0, 100]);
    const handleChangecbd = (event, newValue) => {
        setValuecbd(newValue);
    };


    const handleClickSnackbar = () => {
        setOpenSnackbar(true);
    };
    const handleCloseSnackbar = (event, reason) => {
    if (reason === 'clickaway') {
        return;
    }
        setOpenSnackbar(false);
    };

    const handleClickOpenVerify = () => {
        setOpenVerify(true);
    };
    const handleCloseVerify = () => {
        setOpenVerify(false);
    };

    const handleClickOpenLogin = () => {
        setOpenLogin(true);
    };
    const handleCloseLogin = () => {
        setOpenLogin(false);
    };
    const [openSignUp, setOpenSignUp] = React.useState(false);
    const handleClickOpenSignUp = () => {
        setOpenSignUp(true);
    };
    const handleCloseSignUp = () => {
        setOpenSignUp(false);
    };

    const registerOpen = option => {
        if ( option === "Register" ) {
        handleCloseLogin();
        handleClickOpenSignUp();
        } else {
        handleCloseSignUp();
        handleCloseLogin();
        }
    }

    const signIn = () => {
        auth.signInWithPopup(googleProvider).then((user) => {
        sessionStorage.setItem("userId", user.uid);
        sessionStorage.setItem("userEmail", user.email);
        sessionStorage.setItem("login", true);
        setUserLogin(true);
        handleClose();
        handleCloseLogin();
        var user2 = auth.currentUser;
        if (user2) {
            cartItemFetch(user2.uid);
        } 
        }).catch(err => console.log(err));
    }

    React.useEffect(() => {
        auth.onAuthStateChanged(function(user) {
        if (user) {
            setUser(user);
            sessionStorage.setItem("userId", user.uid);
            sessionStorage.setItem("userEmail", user.email);
            sessionStorage.setItem("login", true);
            setUserLogin(true);
            cartItemFetch(user.uid);
        } else {
            setUser({});
            sessionStorage.setItem("login", false);
            sessionStorage.setItem("userId", false);
            sessionStorage.setItem("userEmail", false);
            setUserLogin(false);
        }
        });
    }, []);

    React.useEffect(() => {
        if (sessionStorage.getItem("login")) {
        var uid = sessionStorage.getItem("userId");
        firestore.collection("cart").doc(uid)
        .onSnapshot(function(doc) {
            if (typeof(doc.data())  == 'undefined') {
                setTotalItems(0);
            } else {
                setTotalItems(doc.data().items);
            }
        });
        }
    }, []);

    const cartItemFetch = (uid) => {
        firestore.collection("cart").doc(uid)
        .onSnapshot(function(doc) {
            if (typeof(doc.data())  == 'undefined') {
                setTotalItems(0);
            } else {
                setTotalItems(doc.data().items);
            }
        });
    }





    const handleClick = () => {
        setOpen(true);
    };
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
        return;
        }
        setOpen(false);
    };

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };
    const primaryOptions = {
        type      : 'loop',
        height     : 350,
        perPage   : 1,
        perMove   : 1,
        gap       : '1rem',
        autoplay     : true,
        pagination: false,
    };


    React.useEffect(() => {
        axios.get(`${API_SERVICE}/api/v1/main/edibles`)
        .then(response => {
            setproducts(response.data);
            setloading(false);
        })
        .catch(err => console.log(err))
    }, []);


    const addItem = (product) => {
        auth.onAuthStateChanged(function(user) {
        if (!user) {
            handleClickOpenLogin();
        } else {
            if (size === "") {

            } else {
                var uid = sessionStorage.getItem("userId");
                var docRef = firestore.collection("cart").doc(uid);
                docRef.get().then(function(doc) {
                    if (doc.exists) {
                        var items = doc.data().items;
                        if (items === 10000) {
                            handleClick();
                            setMessage('Maximum Items in Cart Exceeded');
                        } else {
                            // Send Item to the Card in Database
                            var uploadData = {
                                productId: product._id,
                                userId: uid,
                                size: size,
                                product
                            }
                            axios.post(`${API_SERVICE}/api/v1/main/additemtocart`, uploadData)
                                .then((response) => {
                                    if (response.status === 200) {
                                        setLoading2(true);
                                        refreshList();
                                        handleClick();
                                        setopencartdrawel(true);
                                        setMessage('Item Added to Cart');
                                        items = items + 1;
                                        docRef.set({
                                            items 
                                        }, { merge: true });
                                    } else if (response.status === 201) {
                                        handleClick();
                                        setMessage('Item Already Added to Cart');
                                    }
                                }).catch(err => console.log(err));
                        }
                        setsize("");
                    } else {
                        docRef.set({
                            items: 1 
                        }, { merge: true });
                        var uploadData = {
                            productId: product._id,
                            userId: uid,
                            size: size,
                            product
                        }
                        axios.post(`${API_SERVICE}/api/v1/main/additemtocart`, uploadData)
                            .then((response) => {
                                if (response.status === 200) {
                                    setLoading2(true);
                                    refreshList();
                                    handleClick();
                                    setopencartdrawel(true);
                                    setMessage('Item Added to Cart');
                                } 
                            }).catch(err => console.log(err));
                        setsize("");
                    }
                }).catch(function(error) {
                    console.log("Error getting document:", error);
                });
            }
        }
        });
    }

    const showProductList = () => {
        return products.map(product => {
            return <ProductList setsize={setsize} addItem={addItem} product={product} key={product._id} />
        })
    }

    const list = (anchor) => (
        <div
          className={clsx(classes.list, {
            [classes.fullList]: anchor === 'top' || anchor === 'bottom',
          })}
          role="presentation"
          onClick={toggleDrawer(anchor, false)}
          onKeyDown={toggleDrawer(anchor, false)}
        >
          <List className="p-2">
            <h4 className="font-weight-bold">Size</h4>
            <div className="mt-2">
                <div className="">
                    <a onClick={() => filterSize('1.8')} className="btn btn-block btn-lg btn-outline-dark text-dark font-weight-bold h5"><div>1.8g</div></a>
                </div>
                <div className="">
                    <a onClick={() => filterSize('8')} className="btn btn-block btn-lg btn-outline-dark text-dark font-weight-bold h5"><div>8g</div></a>
                </div>
            </div>

            <div className="mt-2">
                <div className="">
                    <a onClick={() => filterSize('9')} className="btn btn-block btn-lg btn-outline-dark text-dark font-weight-bold h5"><div>9g</div></a>
                </div>
                <div className="">
                    <a onClick={() => filterSize('222')} className="btn btn-block btn-lg btn-outline-dark text-dark font-weight-bold h5"><div>222g</div></a>
                </div>
            </div>

            <div className="mt-2">
                <div className="">
                    <a onClick={() => filterSize('3.5')} className="btn btn-block btn-lg btn-outline-dark text-dark font-weight-bold h5"><div>3.5g</div></a>
                </div>
                <div className="">
                    <a onClick={() => filterSize('4')} className="btn btn-block btn-lg btn-outline-dark text-dark font-weight-bold h5"><div>4g</div></a>
                </div>
            </div>

            <div className="mt-2">
                <div className="">
                    <a onClick={() => filterSize('269')} className="btn btn-block btn-lg btn-outline-dark text-dark font-weight-bold h5"><div>269g</div></a>
                </div>
                <div className="">
                    <a onClick={() => filterSize('355')} className="btn btn-block btn-lg btn-outline-dark text-dark font-weight-bold h5"><div>355g</div></a>
                </div>
            </div>
            <Divider />
            <h2 className="font-weight-bold"><strong>Category</strong></h2>
            <div className="form-group ">
                <input onClick={() => filterCategory('All')} className="form-check-input" name="group100" type="radio" id="radio100" />
                <label for="radio100" className="form-check-label dark-grey-text">All</label>
            </div>

            <div className="form-group">
                <input onClick={() => filterCategory('Indica')} className="form-check-input" name="group100" type="radio" id="radio101" />
                <label for="radio101" className="form-check-label dark-grey-text">Indica</label>
            </div>

            <div className="form-group">
                <input onClick={() => filterCategory('Sativa')} className="form-check-input" name="group100" type="radio" id="radio102" />
                <label for="radio102" className="form-check-label dark-grey-text">Sativa</label>
            </div>

            <div className="form-group">
                <input onClick={() => filterCategory('Hybrid')} className="form-check-input" name="group100" type="radio" id="radio103" />
                <label for="radio103" className="form-check-label dark-grey-text">Hybrid</label>
            </div>
            <Divider />
            <h2 className="font-weight-bold mt-2"><strong>Sub Category</strong></h2>
            <div className="form-group ">
                <input onClick={() => filterSubCategory('Soft Chews')} className="form-check-input" name="group101" type="radio" id="radio104" />
                <label for="radio104" className="form-check-label dark-grey-text">Soft Chews</label>
            </div>

            <div className="form-group">
                <input onClick={() => filterSubCategory('Chocolates')} className="form-check-input" name="group101" type="radio" id="radio105" />
                <label for="radio105" className="form-check-label dark-grey-text">Chocolates</label>
            </div>

            <div className="form-group">
                <input onClick={() => filterSubCategory('Baked Goods')} className="form-check-input" name="group101" type="radio" id="radio106" />
                <label for="radio106" className="form-check-label dark-grey-text">Baked Goods</label>
            </div>

            <div className="form-group">
                <input onClick={() => filterSubCategory('Beverages')} className="form-check-input" name="group101" type="radio" id="radio107" />
                <label for="radio107" className="form-check-label dark-grey-text">Beverages</label>
            </div>
            <Divider />
            <div className="row mt-2 mr-1 ml-1">
                <div className="col-md-6 col-lg-12 mb-5">
                    <h5 className="font-weight-bold dark-grey-text"><strong>THC Mg</strong></h5>
                        <div className="divider"></div>
                        <PrettoSlider valueLabelDisplay="auto" onChange={filterthc} aria-label="pretto slider" defaultValue={0} />
                </div>

                <div className="col-md-6 col-lg-12 mb-5">
                    <h5 className="font-weight-bold dark-grey-text"><strong>CBD Mg</strong></h5>
                        <div className="divider"></div>
                        <PrettoSlider valueLabelDisplay="auto" onChange={filtercbd} aria-label="pretto slider" defaultValue={0} />
                </div>
            </div>
          </List>
          <Divider />
        </div>
    );

    const signInFacebook = () => {
        auth.signInWithPopup(facebookProvider).then(function(result) {
            sessionStorage.setItem("userId", user.uid);
            sessionStorage.setItem("userEmail", user.email);
            sessionStorage.setItem("login", true);
            setUserLogin(true);
            handleClose();
            var user2 = auth.currentUser;
            if (user2) {
                cartItemFetch(user2.uid);
            } 
        }).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            // ...
            console.log(errorMessage);
        });
    }

    const signUp = () => {
        auth.createUserWithEmailAndPassword(email, password)
        .then((result) => {
            var user = result.user;
            user.updateProfile({
                photoURL: "https://kittyinpink.co.uk/wp-content/uploads/2016/12/facebook-default-photo-male_1-1.jpg",
                displayName: fullName
            })
            .then(() => {
              user.sendEmailVerification().then(function() {
                handleClickOpenVerify();
                handleCloseSignUp();
              }).catch(function(error) {
                  console.log(error);
              });
            })
            .catch(err => console.log(err))
        })
        .catch(function(error) {
            if (error.code === "auth/email-already-in-use") {
              setMessage("This email is already registered");
              setPassword('');
              setEmail('');
            }
        });
    }


    const login = () => {
        auth.signInWithEmailAndPassword(email, password)
        .then(() => {
            auth.onAuthStateChanged(function(user) {
                if (user) {
                    setUser(user);
                    sessionStorage.setItem("userId", user.uid);
                    sessionStorage.setItem("userEmail", user.email);
                    sessionStorage.setItem("login", true);
                    setUserLogin(true);
                    cartItemFetch(user.uid);
                    handleCloseSnackbar();
                } else {
                    console.log("No User");
                }
            });
        })
        .catch(function(error) {
            var errorCode = error.code;
            console.log(errorCode);
            if ( errorCode === "auth/wrong-password" ) {
              setMessage("Wrong Password");
              setPassword('');
              setEmail('');
              handleClickSnackbar();
            } else if ( errorCode === "auth/user-not-found" ) {
              setMessage("No User Found");
              setPassword('');
              setEmail('');
              handleClickSnackbar();
            }
        });
    }


    return (
        <>
            <Snackbar
                anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
                }}
                open={open}
                autoHideDuration={3000}
                onClose={handleClose}
                action={
                <React.Fragment>
                    <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
                    <CloseIcon fontSize="small" />
                    </IconButton>
                </React.Fragment>
                }
            >
                <Alert onClose={handleClose} severity="success">
                    <h4 style={{ color: '#fff' }}>{message}</h4>
                </Alert>
            </Snackbar>

            <BrowserView>
                <Dock position='right' isVisible={opencartdrawel}>
                    <div className="container mt-2 ">
                        <center>
                            <button style={{ fontSize: '2vh' }} onClick={() => setopencartdrawel(false)} className="btn btn-outline-danger w-75">Close</button>
                        {
                            loading2 === true ? (
                                <center style={{ marginTop: '10%' }}>
                                    <CircularProgress />
                                </center>
                            ) : (
                                products2 && products2.length ? (
                                <>
                                    <div className="ml-4 ex3">
                                        <strong>HST 13% TAXES INCLUDED</strong> <br /> <hr />
                                        <strong>Total $ {totalprice} CAD</strong>
                                        <hr />
                                        {showProductList2()}
                                    </div>
                                    <br />
                                    <center>
                                        <a href="/cart" style={{ fontSize: '2vh' }} className="btn btn-lg btn-lg btn-success">
                                            Proceed to Cart
                                        </a>
                                    </center>
                                </>
                                ) : (
                                    <>
                                        <center style={{ marginTop: '5%' }}>
                                            <h1>

                                            </h1>
                                        </center>
                                    </>
                                )
                            )
                        }
                        </center>
                    </div>
                </Dock>
            </BrowserView>

            <Snackbar
                anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
                }}
                open={openSnackbar}
                autoHideDuration={1000}
                onClose={handleCloseSnackbar}
                message={message}
                action={
                <React.Fragment>
                    <IconButton size="small" aria-label="close" color="inherit" onClick={handleCloseSnackbar}>
                    <CloseIcon fontSize="small" />
                    </IconButton>
                </React.Fragment>
                }
            />

            <Dialog open={openLogin} onClose={handleCloseLogin} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Login</DialogTitle>
                <DialogContent>
                <DialogContentText style={{ color: '#000000', fontSize: '2vh' }}>
                    Please Logged In to this website, your email and password are end to end encrypted and secure.
                </DialogContentText>
                <TextField value={email} onChange={(event) => setEmail(event.target.value)} style={{ marginTop: '4px' }} id="outlined-basic" label="Email" type="email" variant="outlined" fullWidth />
                <br />
                <TextField value={password} onChange={(event) => setPassword(event.target.value)} style={{ marginTop: '8px', marginBottom: '10px' }} id="outlined-basic" label="Password" type="password" variant="outlined" fullWidth />
                
                <Button onClick={login} style={{ marginTop: '14px', backgroundColor: '#000000', color: '#ffffff', fontSize: '2vh', marginBottom: '10px' }} size="large" startIcon={<ExitToAppIcon />} variant="outlined" fullWidth >Login</Button>
                <center style={{ marginTop: '10px' }}>
                    OR
                </center>
                <Button onClick={signIn} style={{ backgroundColor: '#ffffff', fontSize: '2vh', color: '#000000', size: '10px', height: '50px',  marginTop: '10px' }} variant="contained" fullWidth>
                    <img alt="DocsUp" src="https://img.icons8.com/color/28/000000/google-logo.png"/>
                    Continue with Google
                </Button>
                <Button onClick={signInFacebook} style={{ backgroundColor: '#ffffff', fontSize: '2vh', color: '#000000', size: '10px', height: '50px',  marginTop: '10px' }} variant="contained" fullWidth>
                    <img alt="DocsUp" src="https://img.icons8.com/color/28/000000/facebook-new.png"/>
                    Continue with Facebook
                </Button>
                
                <center style={{ marginTop: '20px', marginBottom: '10px' }}>
                    <a onClick={() => registerOpen('Register')} href="#!">
                    Don't have an Account Sign Up
                    </a>
                </center>

                </DialogContent>
                <DialogActions>
                <Button onClick={handleCloseLogin} color="primary">
                    Close
                </Button>
                </DialogActions>
            </Dialog>

            <Dialog open={openVerify} onClose={handleCloseVerify} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Email Verification</DialogTitle>
                <DialogContent>
                <DialogContentText style={{ color: '#000000', fontSize: '3vh' }}>
                <center>
                    Please Check Your Email Inbox for the Verification Link
                </center>
                </DialogContentText>
                <center>
                    <img src="https://cdn.dribbble.com/users/4874/screenshots/1776423/inboxiconanimation_30.gif" className="img img-fluid w-50" />
                </center>
                <br />
                <center style={{ marginTop: '20px', marginBottom: '10px' }}>
                    <a onClick={() => registerOpen('Login')} href="#!">
                    Back to Login
                    </a>
                </center>
                </DialogContent>
                <DialogActions>
                <Button onClick={handleCloseVerify} color="primary">
                    Close
                </Button>
                </DialogActions>
            </Dialog>

            <Dialog open={openSignUp} onClose={handleCloseSignUp} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Sign Up</DialogTitle>
                <DialogContent>
                <DialogContentText style={{ color: '#000000', fontSize: '2vh' }}>
                    Please Register into this website, your email and password are end to end encrypted and secure.
                </DialogContentText>
                <TextField value={fullName} onChange={(event) => setFullName(event.target.value)} style={{ marginTop: '4px' }} id="outlined-basic" label="Full Name" type="text" variant="outlined" fullWidth />
                <br />
                <TextField onChange={(event) => setEmail(event.target.value)} style={{ marginTop: '8px', marginBottom: '10px' }} id="outlined-basic" label="Email" type="email" variant="outlined" fullWidth />
                <TextField onChange={(event) => setPassword(event.target.value)} style={{ marginTop: '8px', marginBottom: '10px' }} id="outlined-basic" label="Password" type="password" variant="outlined" fullWidth />
                
                <Button onClick={signUp} style={{ marginTop: '14px', fontSize: '2vh', backgroundColor: '#000000', color: '#ffffff' , marginBottom: '10px' }} size="large" startIcon={<ExitToAppIcon />} variant="outlined" fullWidth >Sign Up</Button>
                
                <center style={{ marginTop: '20px', marginBottom: '10px' }}>
                    <a onClick={() => registerOpen('Login')} href="#!">
                    Already have an account
                    </a>
                </center>

                </DialogContent>
                <DialogActions>
                <Button onClick={handleCloseSignUp} color="primary">
                    Close
                </Button>
                </DialogActions>
            </Dialog>

            <Navigation />
            <div style={{ backgroundColor: 'gainsboro' }} className="jumbotron jumbotron-fluid">
                <div className="container text-center">
                    <h2 className="display-4 font-weight-bold">Edibles</h2>
                </div>
            </div>
            <section className="m-4">
            <BrowserView>
            <div className="row pt-4">
                <div className="col-lg-3">
                    <div className="">
                        <div className="row">
                            <div className="col-md-6 col-lg-12 mb-5">
                                <h2 className="font-weight-bold dark-grey-text"><strong>Order By Size</strong></h2>
                                    <div className="divider"></div>
                                    <div className="row mt-2">
                                        <div className="col-md">
                                            <a onClick={() => filterSize('1.8')} className="btn btn-block btn-lg btn-outline-dark text-dark font-weight-bold h5"><div>1.8g</div></a>
                                        </div>
                                        <div className="col-md">
                                            <a onClick={() => filterSize('8')} className="btn btn-block btn-lg btn-outline-dark text-dark font-weight-bold h5"><div>8g</div></a>
                                        </div>
                                    </div>

                                    <div className="row mt-2">
                                        <div className="col-md">
                                            <a onClick={() => filterSize('9')} className="btn btn-block btn-lg btn-outline-dark text-dark font-weight-bold h5"><div>9g</div></a>
                                        </div>
                                        <div className="col-md">
                                            <a onClick={() => filterSize('222')} className="btn btn-block btn-lg btn-outline-dark text-dark font-weight-bold h5"><div>222g</div></a>
                                        </div>
                                    </div>

                                    <div className="row mt-2">
                                        <div className="col-md">
                                            <a onClick={() => filterSize('3.5')} className="btn btn-block btn-lg btn-outline-dark text-dark font-weight-bold h5"><div>3.5g</div></a>
                                        </div>
                                        <div className="col-md">
                                            <a onClick={() => filterSize('4')} className="btn btn-block btn-lg btn-outline-dark text-dark font-weight-bold h5"><div>4g</div></a>
                                        </div>
                                    </div>

                                    <div className="row mt-2">
                                        <div className="col-md">
                                            <a onClick={() => filterSize('269')} className="btn btn-block btn-lg btn-outline-dark text-dark font-weight-bold h5"><div>269g</div></a>
                                        </div>
                                        <div className="col-md">
                                            <a onClick={() => filterSize('355')} className="btn btn-block btn-lg btn-outline-dark text-dark font-weight-bold h5"><div>355g</div></a>
                                        </div>
                                    </div>
                            </div>

                            <div className="col-md-6 col-lg-12 mb-5">
                                <h2 className="font-weight-bold dark-grey-text"><strong>Category</strong></h2>
                                    <div className="divider"></div>

                                    <div className="form-group ">
                                        <input onClick={() => filterCategory('All')} className="form-check-input" name="group100" type="radio" id="radio100" />
                                        <label for="radio100" className="form-check-label dark-grey-text">All</label>
                                    </div>

                                    <div className="form-group">
                                        <input onClick={() => filterCategory('Indica')} className="form-check-input" name="group100" type="radio" id="radio101" />
                                        <label for="radio101" className="form-check-label dark-grey-text">Indica</label>
                                    </div>

                                    <div className="form-group">
                                        <input onClick={() => filterCategory('Sativa')} className="form-check-input" name="group100" type="radio" id="radio102" />
                                        <label for="radio102" className="form-check-label dark-grey-text">Sativa</label>
                                    </div>

                                    <div className="form-group">
                                        <input onClick={() => filterCategory('Hybrid')} className="form-check-input" name="group100" type="radio" id="radio103" />
                                        <label for="radio103" className="form-check-label dark-grey-text">Hybrid</label>
                                    </div>

                            </div>

                            <div className="col-md-6 col-lg-12 mb-5">
                                <h2 className="font-weight-bold dark-grey-text"><strong>Sub Category</strong></h2>
                                    <div className="divider"></div>

                                    <div className="form-group ">
                                        <input onClick={() => filterSubCategory('Soft Chews')} className="form-check-input" name="group101" type="radio" id="radio104" />
                                        <label for="radio104" className="form-check-label dark-grey-text">Soft Chews</label>
                                    </div>

                                    <div className="form-group">
                                        <input onClick={() => filterSubCategory('Chocolates')} className="form-check-input" name="group101" type="radio" id="radio105" />
                                        <label for="radio105" className="form-check-label dark-grey-text">Chocolates</label>
                                    </div>

                                    <div className="form-group">
                                        <input onClick={() => filterSubCategory('Baked Goods')} className="form-check-input" name="group101" type="radio" id="radio106" />
                                        <label for="radio106" className="form-check-label dark-grey-text">Baked Goods</label>
                                    </div>

                                    <div className="form-group">
                                        <input onClick={() => filterSubCategory('Beverages')} className="form-check-input" name="group101" type="radio" id="radio107" />
                                        <label for="radio107" className="form-check-label dark-grey-text">Beverages</label>
                                    </div>

                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-6 col-lg-12 mb-5">
                                <h5 className="font-weight-bold dark-grey-text"><strong>THC Mg</strong></h5>
                                    <div className="divider"></div>
                                    <PrettoSlider min={0} max={20} valueLabelDisplay="auto" onChange={filterthc} aria-label="pretto slider" defaultValue={0} />
                            </div>

                            <div className="col-md-6 col-lg-12 mb-5">
                                <h5 className="font-weight-bold dark-grey-text"><strong>CBD Mg</strong></h5>
                                    <div className="divider"></div>
                                    <PrettoSlider min={0} max={20} valueLabelDisplay="auto" onChange={filtercbd} aria-label="pretto slider" defaultValue={0} />
                            </div>
                        </div>
                    </div>
                </div>

                <div className="col-lg-9">
                    <section className="section pt-4">
                        <div className="row">
                            {
                                loading === true ? (
                                    <center style={{ marginTop: '10%', marginLeft: '50%' }}>
                                        <CircularProgress />
                                    </center>
                                ) : (
                                    <>
                                        {showProductList()}
                                    </>
                                )
                            }
                        </div>
                    </section>
                </div>
            </div>
            </BrowserView>

            <MobileView>
                {['left'].map((anchor) => (
                    <React.Fragment key={anchor}>
                    <Button
                        variant="contained"
                        size="large"
                        onClick={toggleDrawer(anchor, true)}
                        startIcon={<FilterListIcon />}
                        fullWidth
                        style={{ fontSize: '2vh', backgroundColor: '#fff', color: '#000' }}
                    >
                        FILTER
                    </Button>
                    <SwipeableDrawer
                        anchor={anchor}
                        open={state[anchor]}
                        onClose={toggleDrawer(anchor, false)}
                        onOpen={toggleDrawer(anchor, true)}
                    >
                        {list(anchor)}
                    </SwipeableDrawer>
                    </React.Fragment>
                ))}
                <div className="row">
                    <div className="col">
                        <section className="section pt-2">
                            <div className="row">
                                {
                                    loading === true ? (
                                        <center style={{ marginTop: '10%', marginLeft: '50%' }}>
                                            <CircularProgress />
                                        </center>
                                    ) : (
                                        <>
                                            {showProductList()}
                                        </>
                                    )
                                }
                            </div>
                        </section>
                    </div>
                </div>
            </MobileView>
            </section>
            <Footer />
        </>
    )
}

export default Edibles
