import firebase from "firebase/app";
import "firebase/storage";
import "firebase/database";
import "firebase/auth";
import "firebase/messaging";
import "firebase/analytics";
import 'firebase/firestore';

var firebaseConfig = {
    apiKey: "AIzaSyB15nNLuNptb9kJAKAxLEvcEnc2crQAgYs",
    authDomain: "canabist-a51f5.firebaseapp.com",
    databaseURL: "https://canabist-a51f5-default-rtdb.firebaseio.com",
    projectId: "canabist-a51f5",
    storageBucket: "canabist-a51f5.appspot.com",
    messagingSenderId: "697730579964",
    appId: "1:697730579964:web:cecae01f783707622fc679",
    measurementId: "G-6RZ21TY3RL"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();


const storage = firebase.storage();
const database = firebase.database();
const auth = firebase.auth();
const firestore = firebase.firestore();

// Authentication for Google
var googleProvider = new firebase.auth.GoogleAuthProvider();
// Authentication for Facebook
var facebookProvider = new firebase.auth.FacebookAuthProvider();
// Authentication for Twitter
var twitterProvider = new firebase.auth.TwitterAuthProvider();

export {
    firestore, auth, googleProvider, facebookProvider, twitterProvider, database, storage, firebase as default
}