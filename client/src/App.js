import './App.css';
import { BrowserRouter as Router, Route } from "react-router-dom";
// Components
import Home from './components/Home';
import Products from './components/Products';
import Prerolls from './components/Prerolls';
import Vapes from './components/Vapes';
import Extracts from './components/Extracts';
import Edibles from './components/Edibles';
import Tropicals from './components/Tropicals';
import Cart from './components/Cart';
import Checkout from './components/Checkout';
import Profile from './components/Profile';
import AdminDashboard from './components/admin/Orders';
import OrderDetails from './components/admin/OrderInfo';
import Admin from './components/admin/SignIn';
import Verifyage from './components/Verifyage';
import NewProduct from './components/admin/NewProduct';
import NewPreRolls from './components/admin/NewPreRolls';
import NewVapes from './components/admin/NewVapes';
import NewExtracts from './components/admin/NewExtracts';
import NewEdibles from './components/admin/NewEdibles';
import NewTropicals from './components/admin/NewTropicals';
import FavoriteItem from './components/FavoriteItem';
import Payment from './components/Payment';
import Thankyou from './components/Thankyou';
import TrackOrder from './components/TrackOrder';
import RecentBuys from './components/RecentBuys';

function App() {
  return (
    <>
      <Router>
        <Route path="/" exact component={Verifyage} />
        <Route path="/home" exact component={Home} />
        <Route path="/products" exact component={Products} />
        <Route path="/prerolls" exact component={Prerolls} />
        <Route path="/payment" component={Payment} />
        <Route path="/trackorder" component={TrackOrder} />
        <Route path="/recentbuys" component={RecentBuys} />
        <Route path="/thankyou" component={Thankyou} />
        <Route path="/favoriteitem" exact component={FavoriteItem} />
        <Route path="/vapes" exact component={Vapes} />
        <Route path="/extracts" exact component={Extracts} />
        <Route path="/edibles" exact component={Edibles} />
        <Route path="/tropicals" exact component={Tropicals} />
        <Route path="/cart" exact component={Cart} />
        <Route path="/checkout" exact component={Checkout} />
        <Route path="/admin" exact component={Admin} />
        <Route path="/profile" exact component={Profile} />
        <Route path="/admin/orders" exact component={AdminDashboard} />
        <Route path="/admin/orderinfo" exact component={OrderDetails} />
        <Route path="/admin/addproducts" exact component={NewProduct} />
        <Route path="/admin/addprerolls" exact component={NewPreRolls} />
        <Route path="/admin/addvapes" exact component={NewVapes} />
        <Route path="/admin/addextracts" exact component={NewExtracts} />
        <Route path="/admin/addedibles" exact component={NewEdibles} />
        <Route path="/admin/addtropicals" exact component={NewTropicals} />
      </Router>
    </>
  );
}

export default App;
